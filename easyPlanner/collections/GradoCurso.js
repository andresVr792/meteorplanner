GradoCurso=new Mongo.Collection('gradoCurso');

GradoCurso.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

GradoCursoSchema=new SimpleSchema({
    text:{
        type: String,
        label: "Texto"
    },
    value:{
        type: String,
        label: "Valor"
    },
    subnivel:{
        type: String,
        label: "Subnivel"
    }
});
Meteor.methods({

    deleteGradoCurso: function (id) {
        GradoCurso.remove(id);
    }
});
GradoCurso.attachSchema( GradoCursoSchema);