CriteriosEvaluacion=new Mongo.Collection('criteriosEvaluacion');

CriteriosEvaluacion.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});