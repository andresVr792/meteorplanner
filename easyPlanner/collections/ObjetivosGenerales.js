ObjetivosGenerales=new Mongo.Collection('objetivosGenerales');

ObjetivosGenerales.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});
bloque=new SimpleSchema({
    nombre:{
        type: String,
        label: "Bloque"
    }
});

ObjetivosGeneralesSchema=new SimpleSchema({
    areaConocimiento:{
        type: String,
        label: "Area Conocimiento"
    },

    objetivo:{
        type: String,
        label: "objetivo"
    },
    bloques:{
        type:[bloque],
        label: "bloques",
        optional:true

    }


});
Meteor.methods({

    deleteObjetivosGenerales: function (id) {
        ObjetivosGenerales.remove(id);
    }
});
ObjetivosGenerales.attachSchema( ObjetivosGeneralesSchema);