
Meteor.users.allow({
    insert(userId, doc) {
        return true;
    },
    update(userId, doc, fields, modifier) {
        return true;
    },
    remove(userId, doc) {
        return true;
    },
});



Meteor.methods({
    crearUsuario: function (email, apellido,apellido2,nombre, midname,areaC, password,subnivel) {
        let user_id = Accounts.createUser({
            password: password,
            email: email,
            profile: {
                lastName: apellido,
                lastName2: apellido2,
                firstName: nombre,
                midleName: midname,
                areaConocimiento: areaC,
                subnivel: subnivel
            }
        });
        console.log(user_id);
        return user_id;
    },
    eliminarUsuario: function (user_id) {
        Meteor.users.remove({'_id': user_id});
    },
    setAllPasswords(setCedula, newPassword) {
        const users = Meteor.users.find({}).fetch();
        for (const user of users) {
            if (setCedula) {
                Accounts.setPassword(user._id, user.profile.cedula);
            }else{
                Accounts.setPassword(user._id, newPassword);
            }
        }
    }
});