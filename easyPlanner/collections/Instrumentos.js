Instrumentos=new Mongo.Collection('instrumentos');

Instrumentos.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});
