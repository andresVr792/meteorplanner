Paralelo=new Mongo.Collection('paralelo');

Paralelo.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

ParaleloSchema=new SimpleSchema({
    nombreParalelo:{
        type: String,
        label: "Texto"
    }
});
Meteor.methods({

    deleteParalelo: function (id) {
        Paralelo.remove(id);
    }
});
Paralelo.attachSchema( ParaleloSchema);