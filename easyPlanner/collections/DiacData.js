DiacData=new Mongo.Collection('diac');

DiacData.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});