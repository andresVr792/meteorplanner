DimensionesIntegradores=new Mongo.Collection('dimensionIntegradores');

DimensionesIntegradores.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});