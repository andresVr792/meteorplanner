TipoActividad=new Mongo.Collection('tipoActividad');

TipoActividad.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});