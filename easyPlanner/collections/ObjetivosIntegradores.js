ObjetivosIntegradores=new Mongo.Collection('objetivosIntegradores');

ObjetivosIntegradores.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});