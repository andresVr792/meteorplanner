DocentesAnual=new Mongo.Collection('docentesAnual');

DocentesAnual.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

DocentesAnualSchema=new SimpleSchema({
    nombre:{
        type: String,
        label: "Nombre"
    }
});
Meteor.methods({

    deleteDocentesAnual: function (id) {
        DocentesAnual.remove(id);
    }
});
DocentesAnual.attachSchema( DocentesAnualSchema);