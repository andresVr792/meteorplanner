MetasComprension=new Mongo.Collection('metasComprension');

MetasComprension.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});
inteligencias=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
desempenosComprension=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    },
    detalle:{
        type:String,
        optional:true
    }

});
evaluacionContinua=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
MetasComprensionSchema=new SimpleSchema({
    texto:{
        type: String,
        optional:true

    },
    metaComprension:{
        type: Number,
        optional:true
    },
    tipo:{
        type: String,
        optional:true
    },
    idPlanAnual:{
        type: String,
        optional:true

    },
    inteligencias:{
        type:[inteligencias],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    desempenosComprension:{
        type:[desempenosComprension],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    evaluacionContinua:{
        type:[evaluacionContinua],
        optional:true,
        autoform:{
            type:"hidden"
        }
    }

});
Meteor.methods({

    iniciarlizarContenidoMetas: function (idPlanAnual,texto,metaComprension) {
        MetasComprension.insert({

            texto:texto,
            idPlanAnual: idPlanAnual,
            inteligencias:[],
            metaComprension:metaComprension,
            evaluacionContinua:[]
        });

    },
    eliminarContenidoMetas: function (id) {
        MetasComprension.remove(id);
    },
    actualizarTexto:function (id,texto) {
        MetasComprension.update({_id: id}, {
            $set: {
                texto:texto

            }

        });
    },
    actualizarTipo:function (id,tipo) {
        MetasComprension.update({_id: id}, {
            $set: {
                tipo:tipo

            }

        });
    },
    actualizarMeta:function (id,orden) {
        MetasComprension.update({_id: id}, {
            $set: {
                metaComprension:orden

            }

        });
    },
    updatePushInteligencias:function (id,inteligencia) {
        MetasComprension.update({_id: id}, {
            $push: {
                inteligencias: {
                    $each: [{
                        texto: inteligencia
                    }]
                }

            }

        });
    },
    updatePullInteligencias:function (id,inteligencias) {
        MetasComprension.update({_id: id}, {

            $pull: {
                inteligencias: {
                    $in: [{
                        texto: inteligencias
                    }]
                }

            }

        });
    },

    actualizarDesempenoPush:function (idCriterio,actividad,detalle) {

        MetasComprension.update({_id: idCriterio._id}, {

            $push: {
                desempenosComprension: {
                    $each: [{
                        texto: actividad,
                        detalle: detalle
                    }]
                }

            }

        });

    },
    actualizarDesempenoPull:function (idCriterio,actividad,detalle) {
        MetasComprension.update({_id: idCriterio._id}, {

            $pull: {
                desempenosComprension: {
                    $in: [{
                        texto: actividad,
                        detalle: detalle
                    }]
                }

            }

        });
    },
    updatePushEvaluacion:function (id,evaluacion) {
        MetasComprension.update({_id: id}, {
            $push: {
                evaluacionContinua: {
                    $each: [{
                        texto: evaluacion
                    }]
                }

            }

        });
    },
    updatePullEvaluacion:function (id,evaluacion) {
        MetasComprension.update({_id: id}, {

            $pull: {
                evaluacionContinua: {
                    $in: [{
                        texto: evaluacion
                    }]
                }

            }

        });
    },

});
MetasComprension.attachSchema( MetasComprensionSchema);