Asignaturas=new Mongo.Collection('asignaturas');

Asignaturas.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

AsignaturasSchema=new SimpleSchema({
    text:{
        type: String,
        label: "Texto"
    },
    value:{
        type: String,
        label: "Valor"
    },
    area:{
        type: String,
        label: "Area"
    }
});
Meteor.methods({

    deleteAsignaturas: function (id) {
        Asignaturas.remove(id);
    }
});
Asignaturas.attachSchema( AsignaturasSchema);