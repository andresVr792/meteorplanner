ContenidosConceptualesUnidad=new Mongo.Collection('contenidosConceptualesUnidad');

ContenidosConceptualesUnidad.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});
actividades=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    },
    detalle:{
        type:String,
        optional:true
    }

});
adjunto=new SimpleSchema({
    idAdjunto:{
        type:String,
        optional:true
    },
    nombre:{
        type:String,
        optional:true
    }
    ,
    extension:{
        type:String,
        optional:true
    },
    linkDescarga:{
        type:String,
        optional:true
    },

    recorrido:{
        type:String,
        optional:true
    }

});
comentario=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    },
    recorrido:{
        type:String,
        optional:true
    }

});
rtis=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    },
    instrumento:{
        type:String,
        optional:true
    }
});
ContenidosConceptualesUnidadSchema=new SimpleSchema({
    texto:{
        type: String,
        optional:true

    },
    producto:{
        type: String,
        optional:true

    },
    orden:{
        type: Number,
        optional:true
    },
    idPlanAnual:{
        type: String,
        optional:true

    },
    idUnidad:{
        type: String,
        optional:true


    },
    duracionContenido:{
        type: Number,
        optional:true
    },
    actividades:{
        type:[actividades],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    rtis:{
        type:[rtis],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    estado:{
        type:String,
        optional:true,


    },
    adjunto:{
        type:[adjunto],
        optional:true


    },
    comentario:{
        type:[comentario],
        optional:true


    },
    textoEstado:{
        type:String,
        optional:true


    },

});
Meteor.methods({

    iniciarlizarContenido: function (idPlanAnual,idUnidad,texto,producto,duracion) {
        ContenidosConceptualesUnidad.insert({

            texto:texto,
            producto:producto,
            idPlanAnual: idPlanAnual._id,
            idUnidad:idUnidad._id,
            duracionContenido:duracion,
            rtis:[],
            actividades:[],
            estado:'text-red',
            textoEstado:'No completado',
            adjunto:'',
            comentario:''
        });

    },
    eliminarContenido: function (id) {
        ContenidosConceptualesUnidad.remove(id);
    },
    actualizarProducto:function (id,producto) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $set: {
                producto:producto

            }

        });
    },
    actualizarEstado:function (id,estado) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $set: {
                estado:estado

            }

        });
    },
    actualizarTextoEstado:function (id,textoEstado) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $set: {
                textoEstado:textoEstado

            }

        });
    },

    actualizarAdjuntoPush:function (id,adjunto,recorrido ) {
        if(adjunto!=null)
        ContenidosConceptualesUnidad.update({_id: id}, {
            $push: {
                adjunto: {
                    $each: [{
                        idAdjunto: adjunto._id,
                        nombre: adjunto.name,
                        extension:adjunto.extension,
                        linkDescarga:adjunto.link,
                        recorrido:recorrido
                    }]
                }

            }
        });
    },
    actualizarComentarioPush:function (id,comentario,recorrido) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $push: {
                comentario: {
                    $each: [{
                        texto: comentario,
                        recorrido: recorrido
                    }]
                }

            }
        });
    },
    actualizarComentarioPull:function (id,comentario) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $pull: {
                comentario: {
                    $in: [{
                        texto: comentario,
                        detalle: recorrido
                    }]
                }

            }
        });
    },
    actualizarDuracionContenidoConceptualUnidad:function (id,duracion) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $set: {
                duracionContenido:duracion

            }

        });
    },
    actualizarOrden:function (id,orden) {
        ContenidosConceptualesUnidad.update({_id: id}, {
            $set: {
                orden:orden

            }

        });
    },
    actualizarActividadPush:function (idCriterio,actividad,detalle) {

        ContenidosConceptualesUnidad.update({_id: idCriterio._id}, {

            $push: {
                actividades: {
                    $each: [{
                        texto: actividad,
                        detalle: detalle
                    }]
                }

            }

        });

    },
    actualizarActividadPull:function (idCriterio,actividad,detalle) {
        ContenidosConceptualesUnidad.update({_id: idCriterio._id}, {

            $pull: {
                actividades: {
                    $in: [{
                        texto: actividad,
                        detalle: detalle
                    }]
                }

            }

        });
    },
    actualizarRTIPush:function (idUnidad,rti,instrumento) {
        ContenidosConceptualesUnidad.update({_id: idUnidad._id}, {
            $push: {
                rtis: {
                    $each: [{
                        texto: rti,
                        instrumento:instrumento
                    }]
                }

            }

        });

    },
    actualizarRTIPull:function (idUnidad,rti,instrumento) {
        ContenidosConceptualesUnidad.update({_id: idUnidad._id}, {

            $pull: {
                rtis: {
                    $in: [{
                        texto: rti,
                        instrumento:instrumento
                    }]
                }

            }

        });
    }
});
ContenidosConceptualesUnidad.attachSchema( ContenidosConceptualesUnidadSchema);