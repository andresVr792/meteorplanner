Actividades=new Mongo.Collection('actividades');

Actividades.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

ActividadesSchema=new SimpleSchema({
    text:{
        type: String,
        label: "Texto"
    }
});

Actividades.attachSchema(ActividadesSchema);