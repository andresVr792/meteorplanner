PlanAnual=new Mongo.Collection('planAnual');

PlanAnual.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

objetivosAreaConocimiento=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
/**
 * campos proyecto de comprensión
 */
hiloConductor=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
metaComprension=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    },
    orden:{
        type:Number,
        optional:true
    }
});

/**
 * fin campos proyecto de comprensión
 */
/**
 *Inicio campos leccion cooperativa
 */
academicos=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
habilidadesSociales=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
roles=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
material=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
observadorPor=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
otros=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
evaluacionProceso=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    },
    tipo:{
        type:String,
        optional:true
    }
});

/**
 * Fin campos leccion cooperativa
 */

 bibliografia=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
ejesTransversales=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
objetivosIntegradores=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
curriculoIntegrador=new SimpleSchema({
    ejesDesarrolloAprendizaje:{
        type: String,
        optional:true
    },
    ambitosDesarrolloAprendizaje:{
        type: String,
        optional:true
    }
});
docen=new SimpleSchema({
    nombre:{
        type: String,
        optional:true
    }
});
paralelo=new SimpleSchema({
    nombre:{
        type: String,
        optional:true
    }
});

autor=new SimpleSchema({
    idDocente:{
        type: String,
        autoValue: function () {
            return Meteor.userId();
        }
    },
    firstName:{
        type:String,
        autoValue:function () {
            return Meteor.user().profile.firstName;
        }
    },
    midleName:{
        type:String,
        autoValue:function () {
            return Meteor.user().profile.midleName;
        }
    },
    lastName:{
        type:String,
        autoValue:function () {
            return Meteor.user().profile.lastName;
        }
    },
    lastname2:{
        type:String,
        autoValue:function () {
            return Meteor.user().profile.lastName2;
        }
    }

});
compartido=new SimpleSchema({
    idDocente:{
        type: String,
        optional:true
    },
    correoElectronico:{
        type:String,
        optional:true
    }
});
PlanAnualSchema=new SimpleSchema({

    nombre:{
        type: String,
        label: "Nombre"
    },
    tipo:{
        type: String,
        label: "Tipo"
    },

    fechaCreacion:{
        type: Date,
        label: "Creado a las",
        autoValue: function () {
            return new Date()
        },
        autoform:{
            type:"hidden"
        }
    },
    fechaModificacion:{
        type: Date,
        label: "Última modificación",
        autoValue: function () {
            return new Date()
        },
        autoform:{
            type:"hidden"
        }

    },
    curriculoIntegrador:{
     type:[curriculoIntegrador],
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    autor:{
        type:[autor],
        autoform:{
            type:"hidden"
        }
    },
    docentes:{
        type:[docen],
        autoform:{
            type:"hidden"
        },
        optional:true

    },
    compartido:{
        type:[compartido],
        autoform:{
            type:"hidden"
        },
        optional:true

    },
    paralelos:{
        type:[paralelo],
        autoform:{
            type:"hidden"
        },
        optional:true

    },
    cargaHorariaSemanal:{
        type:Number,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    evaluacionAprendizajeImprevistos:{
        type:Number,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    totalSemanasClase:{
        type:Number,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    totalPeriodos:{
        type:Number,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    areaConocimiento:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    asignatura:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    subnivelEducativo:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    gradoCurso:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    }
    ,
    ejesTransversales:{
        type:[ejesTransversales],
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    objetivosAreaConocimiento:{
        type:[objetivosAreaConocimiento],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    objetivosIntegradores:{
        type:[objetivosIntegradores],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    bibliografia:{
        type:[bibliografia],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    planCompletado:{
        type:Number,
        optional:true
    },
    /**
     * proyecto de comprensión
     */
    topicoGenerativo:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },hiloConductor:{
        type:[hiloConductor],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },anioLectivo:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },quimestre:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },metaComprension:{
        type:[metaComprension],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },


    /**
     * fin proyecto de comprensión
     */
    /**
     * inicio leccion cooperativa
     */
    objetivoAcademico: {
        type: String,
        autoform: {
            type: "hidden"
        },
        optional:true

    },
    objetivoHabilidadSocial: {
        type: String,
        autoform: {
            type: "hidden"
        }
        ,
        optional:true
    },
    tamanioGrupo:{
            type:Number,
            autoform:{
                type:"hidden"
            },
            optional:true
        },
    metodoAsignarAlumnos:{
            type:String,
            autoform:{
                type:"hidden"
            },
            optional:true
        },
    roles: {
        type: [roles],
        autoform: {
            type: "hidden"
        },

        optional:true
    },
    distribucionAula:{
                type:String,
                autoform:{
                    type:"hidden"
                },
                optional:true
            },
    material:{
            type:[material],
            autoform:{
                type:"hidden"
            },
            optional:true
        },
    tarea:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    criterioParaExito:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    interdependeciaPositiva:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    responsabilidadIndividual:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    cooperacionIntergrupos:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    comportamientosEsperados:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    procedimientoObservacion:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    observadorPor:{
        type:[observadorPor],
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    intervencionParaAyudarSobreTarea:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    intervencionTrabajoEquipo:{
        type:String,
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    otros:{
        type:[otros],
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    evaluacionProceso:{
        type:[evaluacionProceso],
        autoform:{
            type:"hidden"
        },
        optional:true
    },
    fechaEjecucion:{
        type: Date,
        optional:true,

        autoform:{
            type:"hidden"
        }

    },
    /**
     * fin leccion cooperativa
     */




});
Meteor.methods({

    /**
     * Inicio metodos leccion comperativa
     */
    updateFechaEjecucion:function (id,fecha) {
        PlanAnual.update({_id: id}, {

            $set: {
                fechaEjecucion: fecha
            }

        });
    },
    updateObjetivoAcademico:function (id,objetivoAcademico) {
        PlanAnual.update({_id: id}, {

            $set: {
                objetivoAcademico: objetivoAcademico
            }

        });
    },
    updateObjetivoHabilidadSocial:function (id,objetivoHabilidadSocial) {
        PlanAnual.update({_id: id}, {

            $set: {
                objetivoHabilidadSocial: objetivoHabilidadSocial
            }

        });
    },
    updateTamanioGrupo:function (id,tamanio) {
        PlanAnual.update({_id: id}, {

            $set: {
                tamanioGrupo: tamanio
            }

        });
    },
    updateMetodoAsignarAlumnos:function (id,metodos) {
        PlanAnual.update({_id: id}, {

            $set: {
                metodoAsignarAlumnos: metodos
            }

        });
    },
    updateDistribucionAula:function (id,distribucion) {
        PlanAnual.update({_id: id}, {

            $set: {
                distribucionAula: distribucion
            }

        });
    },
    updateTarea:function (id,tarea) {
        PlanAnual.update({_id: id}, {

            $set: {
                tarea: tarea
            }

        });
    },
    updateCriterioParaExito:function (id,criterio) {
        PlanAnual.update({_id: id}, {

            $set: {
                criterioParaExito: criterio
            }

        });
    },

    updateInterdependeciaPositiva:function (id,interdependecia) {
        PlanAnual.update({_id: id}, {

            $set: {
                interdependeciaPositiva: interdependecia
            }

        });
    },
    updateResponsabilidadIndividual:function (id,responsabilidad) {
        PlanAnual.update({_id: id}, {

            $set: {
                responsabilidadIndividual: responsabilidad
            }

        });
    },
    updateCooperacionIntergrupos:function (id,cooperacion) {
        PlanAnual.update({_id: id}, {

            $set: {
                cooperacionIntergrupos: cooperacion
            }

        });
    },
    updateComportamientosEsperados:function (id,comportamientos) {
        PlanAnual.update({_id: id}, {

            $set: {
                comportamientosEsperados: comportamientos
            }

        });
    },
    updateProcedimientoObservacion:function (id,procedimiento) {
        PlanAnual.update({_id: id}, {

            $set: {
                procedimientoObservacion: procedimiento
            }

        });
    },
    updateIntervencionParaAyudarSobreTarea:function (id,intervencion) {
        PlanAnual.update({_id: id}, {

            $set: {
                intervencionParaAyudarSobreTarea: intervencion
            }

        });
    },
    updateIntervencionTrabajoEquipo:function (id,intervencionTrabajo) {
        PlanAnual.update({_id: id}, {

            $set: {
                intervencionTrabajoEquipo: intervencionTrabajo
            }

        });
    },

    /**
     * Metodos Push Pull
     */
    updatePushRoles:function (id,rol) {
        PlanAnual.update({_id: id}, {
            $push: {
                roles: {
                    $each: [{
                        texto: rol
                    }]
                }

            }

        });
    },

    updatePullRoles:function (id,rol) {
        PlanAnual.update({_id: id}, {

            $pull: {
                roles: {
                    $in: [{
                        texto: rol
                    }]
                }

            }

        });
    },
    updatePushMaterial:function (id,material) {
        PlanAnual.update({_id: id}, {
            $push: {
                material: {
                    $each: [{
                        texto: material
                    }]
                }

            }

        });
    },

    updatePullMaterial:function (id,material) {
        PlanAnual.update({_id: id}, {

            $pull: {
                material: {
                    $in: [{
                        texto: material
                    }]
                }

            }

        });
    },
    updatePushObservadorPor:function (id,observadoPor) {
        PlanAnual.update({_id: id}, {
            $push: {
                observadorPor: {
                    $each: [{
                        texto: observadoPor
                    }]
                }

            }

        });
    },

    updatePullObservadorPor:function (id,observadoPor) {
        PlanAnual.update({_id: id}, {

            $pull: {
                observadorPor: {
                    $in: [{
                        texto: observadoPor
                    }]
                }

            }

        });
    },
    updatePushOtros:function (id,otros) {
        PlanAnual.update({_id: id}, {
            $push: {
                otros: {
                    $each: [{
                        texto: otros
                    }]
                }

            }

        });
    },

    updatePullOtros:function (id,otros) {
        PlanAnual.update({_id: id}, {

            $pull: {
                otros: {
                    $in: [{
                        texto: otros
                    }]
                }

            }

        });
    },
    updatePushEvaluacionProceso:function (id,evaluacion, tipo) {
        PlanAnual.update({_id: id}, {
            $push: {
                evaluacionProceso: {
                    $each: [{
                        texto: evaluacion,
                        tipo:tipo
                    }]
                }

            }

        });
    },

    updatePullEvaluacionProceso:function (id,evaluacion, tipo) {
        PlanAnual.update({_id: id}, {

            $pull: {
                evaluacionProceso: {

                        texto: evaluacion,
                        tipo:tipo

                }

            }

        });
    },
    /**
     * Fin metodos leccion comperativa
     */
    deletePlanAnual: function (id) {
        PlanAnual.remove(id);
    },
    updateTopicoGenerativo:function (id,topico) {
        PlanAnual.update({_id: id}, {

            $set: {
                topicoGenerativo: topico
            }

        });
    },
    updateAnioLectivo:function (id,anio) {
        PlanAnual.update({_id: id}, {

            $set: {
                anioLectivo: anio
            }

        });
    },
    updateQuimestre:function (id,quimestre) {
        PlanAnual.update({_id: id}, {

            $set: {
                quimestre: quimestre
            }

        });
    },
    updateAreaConocimiento:function (id,area) {
        PlanAnual.update({_id: id}, {

            $set: {
                areaConocimiento: area




            }

        });
    },
    updateEjesDesarrolloAprendizaje:function (id,eje) {
        PlanAnual.update({_id: id}, {

            $set: {
                ejesDesarrolloAprendizaje: eje



            }

        });
    },
    updatePlanCompletado:function (id,numero) {
        PlanAnual.update({_id: id}, {

            $set: {
                planCompletado: numero



            }

        });
    }
    ,
    updateAmbitosDesarrolloAprendizaje:function (id,ambito) {
        PlanAnual.update({_id: id}, {

            $set: {
                ambitosDesarrolloAprendizaje: ambito



            }

        });
    },
    updateAsignatura:function (id,asignatura) {
        PlanAnual.update({_id: id}, {

            $set: {
                asignatura: asignatura,




            }

        });
    },

    updatePushDocente:function (id,nombre) {
        PlanAnual.update({_id: id}, {
            $push: {
                docentes: {
                    $each: [{
                        nombre: nombre
                    }]
                }

            }

        });
    },
    updatePullDocente:function (id,nombre) {
        PlanAnual.update({_id: id}, {

            $pull: {
                docentes: {
                    $in: [{
                        nombre: nombre
                    }]
                }

            }

        });
    },
    /**
     * Proyecto de comprensión
     */

    updatePushHiloConductor:function (id,hilo) {
    PlanAnual.update({_id: id}, {
        $push: {
            hiloConductor: {
                $each: [{
                    texto: hilo
                }]
            }

        }

    });
},

    updatePullHiloConductor:function (id,hilo) {
        PlanAnual.update({_id: id}, {

            $pull: {
                hiloConductor: {
                    $in: [{
                        texto: hilo
                    }]
                }

            }

        });
    }, updatePullMetaComprension:function (id,meta,orden) {

            PlanAnual.update({_id: id}, {

                $pull: {
                    metaComprension: {
                        $in: [{
                            texto: meta,
                            orden:orden
                        }]
                    }

                }

            });
        }
        ,
        updatePushMetaComprension:function (id,meta,orden) {
            PlanAnual.update({_id: id}, {
                $push: {
                    metaComprension: {
                        $each: [{
                            texto: meta,
                            orden:orden
                        }]
                    }

                }

            });
        }
    /**
     * Fin proyecto de comprension
     */
    ,
    updatePushCompartido:function (id,idDocente,correoElectronico) {
        PlanAnual.update({_id: id}, {
            $push: {
                compartido: {
                    $each: [{
                        idDocente:idDocente,


                        correoElectronico:correoElectronico
                    }]
                }

            }

        });
    },
    updatePullCompartido:function (id,idDocente,correoElectronico) {
        PlanAnual.update({_id: id}, {

            $pull: {
                compartido: {
                    $in: [{
                        idDocente:idDocente,

                        correoElectronico:correoElectronico
                    }]
                }

            }

        });
    },
    updateGradoCurso:function (id,gradoCurso) {
        PlanAnual.update({_id: id}, {

            $set: {
                gradoCurso: gradoCurso




            }

        });
    },
    updatePushParalelos:function (id,paralelo) {
        PlanAnual.update({_id: id}, {
            $push: {
                paralelos: {
                    $each: [{
                        nombre: paralelo
                    }]
                }

            }

        });
    },
    updatePullParalelos:function (id,paralelo) {
        PlanAnual.update({_id: id}, {

            $pull: {
                paralelos: {
                    $in: [{
                        nombre: paralelo
                    }]
                }

            }

        });
    },
    updateSubnivel:function (id,subnivel) {
        PlanAnual.update({_id: id}, {

            $set: {
                subnivelEducativo: subnivel




            }

        });

    },
    updateTiempoHorasCargaHoraria:function (id,totalHoras,cargaHoraria) {
        PlanAnual.update({_id: id}, {

            $set: {
                totalPeriodos: totalHoras,
                cargaHorariaSemanal:cargaHoraria



            }

        });
    },
    updateTiempoSemanasClaseImprevistos:function (id,semanasClase,evaluacionAprendisaje) {
        PlanAnual.update({_id: id}, {

            $set: {
                totalSemanasClase: semanasClase,
                evaluacionAprendizajeImprevistos:evaluacionAprendisaje



            }

        });
    },
    updatePushObjetivosGenerales:function (id,objetivo) {
        PlanAnual.update({_id: id}, {
            $push: {
                objetivosAreaConocimiento: {
                    $each: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    updatePullObjetivosGenerales:function (id,objetivo) {
        PlanAnual.update({_id: id}, {

            $pull: {
                objetivosAreaConocimiento: {
                    $in: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    updatePushObjetivosIntegradores:function (id,objetivo) {
        PlanAnual.update({_id: id}, {
            $push: {
                objetivosIntegradores: {
                    $each: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    updatePullObjetivosIntegradores:function (id,objetivo) {
        PlanAnual.update({_id: id}, {

            $pull: {
                objetivosIntegradores: {
                    $in: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    updatePushEjesTransersales:function (id,objetivo) {
        PlanAnual.update({_id: id}, {
            $push: {
                ejesTransversales: {
                    $each: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    updatePullEjesTransersales:function (id,objetivo) {
        PlanAnual.update({_id: id}, {

            $pull: {
                ejesTransversales: {
                    $in: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    updatePushBibliografia:function (id,bibliografia) {
        PlanAnual.update({_id: id}, {
            $push: {
                bibliografia: {
                    $each: [{
                        texto: bibliografia
                    }]
                }

            }

        });
    },
    updatePullBibliografia:function (id,bibliografia) {
        PlanAnual.update({_id: id}, {

            $pull: {
                bibliografia: {
                    $in: [{
                        texto: bibliografia
                    }]
                }

            }

        });
    },updatePushCurriculoIntegrador:function (id,ejes,ambitos) {
        PlanAnual.update({_id: id._id}, {
            $push: {
                curriculoIntegrador: {
                    $each: [{
                        ejesDesarrolloAprendizaje: ejes,
                        ambitosDesarrolloAprendizaje:ambitos
                    }]
                }

            }

        });
    },
    updatePullCurriculoIntegrador:function (idplan,ejes,ambitos) {

        PlanAnual.update({_id: idplan._id}, {

            $pull: {
                curriculoIntegrador: {

                    ejesDesarrolloAprendizaje: ejes,
                    ambitosDesarrolloAprendizaje:ambitos

                }

            }

        });
    }
});
PlanAnual.attachSchema( PlanAnualSchema);