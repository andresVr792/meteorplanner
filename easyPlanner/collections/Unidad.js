Unidad=new Mongo.Collection('unidad');

Unidad.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});
adaptacionesCurriculares=new SimpleSchema({
    nee:{
        type:String,
        optional:true
    },
    diac:{
        type:String,
        optional:true
    },
    alumno:{
        type:String,
        optional:true
    },
    diagnostico:{
        type:String,
        optional:true
    }
});
objetivosUnidadPlanificacion=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
contenidosConceptuales=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});

actividades=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
destrezas=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
orientacionesMetodologicas=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
criteriosindicadoresEvaluacion=new SimpleSchema({
    texto:{
        type:String,
        optional:true
    }
});
UnidadSchema=new SimpleSchema({
    titulo:{
        type: String

    },
    idPlanAnual:{
        type: String

    },
    numeroUnidad:{
        type: Number,
        autoform:{
            type:"hidden"
        }

    },
    fechaInicio:{
        type: String,
        optional:true
    },
    fechaFin:{
        type: String,
        optional:true
    }
    ,
    elementoIntegrador:{
        type: String,
        optional:true
    },
    caracterizacionExperienciaAprendizaje:{
        type: String,
        optional:true
    },
    duracionUnidad:{

        type: Number,
        optional:true

    },
    objetivosEspecificos:{
        type:[objetivosUnidadPlanificacion],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    destrezas:{
        type:[destrezas],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    contenidosConceptuales:{
        type:[contenidosConceptuales],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    orientacionesMetodologicas:{
        type:[orientacionesMetodologicas],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    criteriosindicadoresEvaluacion:{
        type:[criteriosindicadoresEvaluacion],
        optional:true,
        autoform:{
            type:"hidden"
        }
    },
    adaptacionesCurriculares:{
        type:[adaptacionesCurriculares],
        optional:true,
        autoform:{
            type:"hidden"
        }
    }


});
Meteor.methods({

    iniciarlizarUnidad: function (idPlanAnual,numeroUnidad,titulo,duracion) {
        Unidad.insert({
            titulo:titulo,
            idPlanAnual: idPlanAnual,
            numeroUnidad:numeroUnidad,
            duracionUnidad:duracion,
            contenidosConceptuales:[],
            objetivosEspecificos:[],
            destrezas:[],
            orientacionesMetodologicas:[],
            criteriosindicadoresEvaluacion:[],
            adaptacionesCurriculares:[]
        });
    },iniciarlizarUnidadElemental: function (idPlanAnual,numeroUnidad,titulo,duracion,finicio,ffin,elementoIntegradorplan,caracterizacionExperiencia) {
        Unidad.insert({
            titulo:titulo,
            idPlanAnual: idPlanAnual,
            numeroUnidad:numeroUnidad,
            duracionUnidad:duracion,
            fechaInicio:finicio,
            fechaFin:ffin,
            elementoIntegrador:elementoIntegradorplan,
            caracterizacionExperienciaAprendizaje:caracterizacionExperiencia,
            destrezas:[],
            criteriosindicadoresEvaluacion:[],
            objetivosEspecificos:[],
            adaptacionesCurriculares:[]

        });
    },
    eliminarUnidad: function (id) {
        Unidad.remove(id);
    },
    actualizarUnidadTitulo:function (idUnidad,titulo) {
        Unidad.update({_id: idUnidad}, {
            $set: {
                titulo:titulo

            }

        });
    },
    actualizarDuracionUnidad:function (idUnidad,duracion) {
        Unidad.update({_id: idUnidad}, {
            $set: {
                duracionUnidad:duracion

            }

        });
    },
    actualizarNumeroUnidad:function (idUnidad,numero) {
        Unidad.update({_id: idUnidad}, {
            $set: {
                numeroUnidad:numero

            }

        });
    },
    actualizarObjetivoEspecificoPush:function (idUnidad,objetivo) {
        Unidad.update({_id: idUnidad}, {
            $push: {
                objetivosEspecificos: {
                    $each: [{
                        texto: objetivo
                    }]
                }

            }

        });

    },
    actualizarObjetivoEspecificoPull:function (idUnidad,objetivo) {
        Unidad.update({_id: idUnidad}, {

            $pull: {
                objetivosEspecificos: {
                    $in: [{
                        texto: objetivo
                    }]
                }

            }

        });
    },
    actualizarContenidoPush:function (idUnidad,destreza) {
        Unidad.update({_id: idUnidad._id}, {
            $push: {
                contenidosConceptuales: {
                    $each: [{
                        texto: destreza
                    }]
                }

            }

        });

    },
    actualizarContenidoPull:function (idUnidad,contenido) {
        Unidad.update({_id: idUnidad._id}, {

            $pull: {
                contenidosConceptuales: {
                    "$in": [{
                        texto: contenido
                    }]
                }

            }

        });
    },
    actualizarDestrezaPush:function (idUnidad,destreza) {
        Unidad.update({_id: idUnidad}, {
            $push: {
                destrezas: {
                    $each: [{
                        texto: destreza
                    }]
                }

            }

        });

    },
    actualizarDestrezaPull:function (idUnidad,contenido) {
        Unidad.update({_id: idUnidad}, {

            $pull: {
                destrezas: {
                    $in: [{
                        texto: contenido
                    }]
                }

            }

        });
    },
    actualizarOrientacionMetodologicaPush:function (idUnidad,orientacion) {
        Unidad.update({_id: idUnidad}, {
            $push: {
                orientacionesMetodologicas: {
                    $each: [{
                        texto: orientacion
                    }]
                }

            }

        });

    },
    actualizarOrientacionMetodologicaPull:function (idUnidad,orientacion) {
        Unidad.update({_id: idUnidad}, {

            $pull: {
                orientacionesMetodologicas: {
                    $in: [{
                        texto: orientacion
                    }]
                }

            }

        });
    },
    actualizarcriteriosindicadoresEvaluacionPush:function (idUnidad,criterio) {
        Unidad.update({_id: idUnidad}, {
            $push: {
                criteriosindicadoresEvaluacion: {
                    $each: [{
                        texto: criterio
                    }]
                }

            }

        });

    },
    actualizarcriteriosindicadoresEvaluacionPull:function (idUnidad,criterio) {
        Unidad.update({_id: idUnidad}, {

            $pull: {
                criteriosindicadoresEvaluacion: {
                    $in: [{
                        texto: criterio
                    }]
                }

            }

        });
    }
    , actualizarAdaptacionCurricularPull:function (idUnidad,nee,diac,alumno,diagnostico) {

        Unidad.update({_id: idUnidad._id}, {

        $pull: {
            adaptacionesCurriculares: {

                    nee: nee,
                    diac:diac,
                    alumno:alumno,
                    diagnostico:diagnostico

            }

        }

    });
},
actualizarAdaptacionCurricularPush:function (idUnidad,nee,diac,alumno,diagnostico) {
    Unidad.update({_id: idUnidad._id}, {
        $push: {
            adaptacionesCurriculares: {
                $each: [{
                    nee: nee,
                    diac:diac,
                    alumno:alumno,
                    diagnostico:diagnostico


                }]
            }

        }

    });

}

});
Unidad.attachSchema( UnidadSchema);