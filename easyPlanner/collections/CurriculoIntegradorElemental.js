
CurriculoIntegradorElemental=new Mongo.Collection('curriculoIntegradorElemental');

CurriculoIntegradorElemental.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});