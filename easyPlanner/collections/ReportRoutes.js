ReportRoutes=new Mongo.Collection('reportRoutes');

ReportRoutes.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});
ReportRoutesSchema=new SimpleSchema({

    nombre:{
        type: String,
        label: "Nombre"
    },
    nombrePlan:{
        type: String,
        label: "nombrePlan",
        optional:true
    },
    ruta:{
        type: String,
        label: "ejesDesarrolloAprendizaje",
        optional:true
    },
    idAutor:{
        type: String,
        label: "ambitosDesarrolloAprendizaje",
        optional:true
    },
    fechaCreacion:{
        type: Date,
        label: "Creado a las",
        autoValue: function () {
            return new Date()
        },
        autoform:{
            type:"hidden"
        }
    }

});
Meteor.methods({
    deleteReporte: function (id) {
        ReportRoutes.remove(id);
    }
});
ReportRoutes.attachSchema( ReportRoutesSchema);