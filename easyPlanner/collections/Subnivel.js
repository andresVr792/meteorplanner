Subnivel=new Mongo.Collection('subnivel');

Subnivel.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

SubnivelSchema=new SimpleSchema({
    text:{
        type: String,
        label: "Texto"
    },
    value:{
        type: String,
        label: "Valor"
    },
    index:{
        type: String,
        label: "Valor"
    }
});
Meteor.methods({

    deleteSubnivel: function (id) {
        Subnivel.remove(id);
    }
});
Subnivel.attachSchema( SubnivelSchema);