Contenidos=new Mongo.Collection('contenidosConceptuales');

Contenidos.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});