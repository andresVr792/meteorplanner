Areas=new Mongo.Collection('areas');

Areas.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});

AreasSchema=new SimpleSchema({
    text:{
        type: String,
        label: "Texto"
    },
    value:{
        type: String,
        label: "Valor"
    }
});
Meteor.methods({

    deleteAreas: function (id) {
        Areas.remove(id);
    }
});
Areas.attachSchema( AreasSchema);