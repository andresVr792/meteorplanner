ObjetivosEspecificos=new Mongo.Collection('objetivosEspecificos');

ObjetivosEspecificos.allow({
    insert:function (userId, doc) {
        return !!userId;

    },
    update:function (userId,doc) {
        return !!userId;
    }
});