Template.AnioLectivo.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('anioLectivo');
        opEditarPlan=Session.get('op-editarPlan');
        /*if(!opEditarPlan=='open') {
         Meteor.call('updateAreaConocimiento', Session.get('currentPlan')._id, Session.get('logedUser').profile.areaConocimiento);
         var area = Areas.findOne({value: Session.get('logedUser').profile.areaConocimiento});
         Session.setPersistent('areaObjTmp', area);
         }*/
    });
});
Template.AnioLectivo.helpers({
        anio: ()=>{
        return AnioLectivo.find({});
},
anioPlan:function(){

    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var anio = PlanAnual.findOne({_id: currentPlanAC._id});
        Session.setPersistent('anioTmp', anio.anioLectivo);
        return PlanAnual.findOne({_id: Session.get('currentPlan')._id});


    }
},
logedUserAnio:function () {
    return "SELECCCIONAR ANIO LECTIVO"

},
modoEdicionAL:()=>{
    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var anio = PlanAnual.findOne({_id: currentPlanAC._id});
        return !anio.anioLectivo;
    }
}
});
Template.AnioLectivo.events({
    'change form#anio #anioLectivo': function(event,template){
        var anioTmp=template.find('#anioLectivo').value;
        Session.setPersistent('anioTmp',anioTmp);
        Meteor.call('updateAnioLectivo',Session.get('currentPlan')._id,anioTmp);
        $('#anioLectivo').val($('#anioLectivo > option:first').val());
        template.find('#anioL').value=anioTmp;
    }
});
