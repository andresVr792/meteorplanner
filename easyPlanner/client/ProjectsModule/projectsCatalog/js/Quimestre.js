Template.Quimestre.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('quimestre');
        opEditarPlan=Session.get('op-editarPlan');
        /*if(!opEditarPlan=='open') {
         Meteor.call('updateAreaConocimiento', Session.get('currentPlan')._id, Session.get('logedUser').profile.areaConocimiento);
         var area = Areas.findOne({value: Session.get('logedUser').profile.areaConocimiento});
         Session.setPersistent('areaObjTmp', area);
         }*/
    });
});
Template.Quimestre.helpers({
        quimestre: ()=>{
        return Quimestre.find({});
},
quimestreP:function(){

    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var quimestreP = PlanAnual.findOne({_id: currentPlanAC._id});
        Session.setPersistent('quimestreTmp', quimestreP.quimestre);
        return PlanAnual.findOne({_id: Session.get('currentPlan')._id});


    }
},
logedUserQuimestre:function () {
    return "SELECCCIONAR QUIMESTRE"

},
modoEdicionQ:()=>{
    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var quimestreP = PlanAnual.findOne({_id: currentPlanAC._id});
        return !quimestreP.quimestre;
    }
}
});
Template.Quimestre.events({
    'change form#quimestre #quimestreSlc': function(event,template){
        var quimestreTmp=template.find('#quimestreSlc').value;
        Session.setPersistent('quimestreTmp',quimestreTmp);
        Meteor.call('updateQuimestre',Session.get('currentPlan')._id,quimestreTmp);
        $('#quimestreSlc').val($('#quimestreSlc > option:first').val());
        template.find('#quimestreL').value=quimestreTmp;
    }
});
