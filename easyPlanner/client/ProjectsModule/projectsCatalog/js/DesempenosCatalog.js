import '../../../../public/plugins/select2/i18n/es';
import '../../../../public/plugins/select2/select2.full.min.js';

Template.DesempenoCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('allActividades');
        if( Session.get('nav-actividades-edit')=='open'){
            Session.set('nav-actividades','open');

            $('#actividades').val( Session.get('actividadEdit'));
            $('#txtAreaDetalleActividad').val( Session.get('actividadDetalleEdit'));
        }else{
            $('#actividades').val('');
            $('#txtAreaDetalleActividad').val( '');
        }

    });
});

Template.DesempenoCatalog.rendered = function () {
    $("#slcActividades").select2({
        placeholder: "Selecciona/busca el desempeño/actividad",
        language: "es",
        tags:true,


    });
    $("#slcMeta").select2({
        placeholder: "Selecciona/busca la meta",
        language: "es",



    });

}
Template.DesempenoCatalog.helpers({
    actividades: function () {
        return Actividades.find();
    },
    metas:function(){
        currentPlanDC=Session.get('currentPlan');
        if(currentPlanDC!=''){
            return PlanAnual.findOne({_id:currentPlanDC._id}).metaComprension;
        }
    }

});
Template.DesempenoCatalog.events({
        'click .close-actividad ': () => {
        Session.set('nav-actividades', '');
Session.set('nav-actividades-edit','');


},
'click .btn-AgregarActividad': function (event, template) {



        var actividadTmp = template.find('#slcActividades').value;
        var metaIndex=$('#slcMeta').prop('selectedIndex');


        var actividades = MetasComprension.find({idPlanAnual: Session.get('currentPlan')._id,texto:actividadTmp}).count();
        if (actividades==0) {

            Meteor.call('iniciarlizarContenidoMetas',Session.get('currentPlan')._id,actividadTmp,metaIndex)
            meta=MetasComprension.findOne({texto:actividadTmp,metaComprension:metaIndex});
            Session.setPersistent('currentMeta',meta);
            Session.set('nav-actividades-comprension','open');
        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center"> ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }



    Session.set('nav-actividades', '');

    return false;



},


});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}