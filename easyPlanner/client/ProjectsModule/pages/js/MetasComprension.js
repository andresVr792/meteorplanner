Template.MetasComprension.onCreated(function(){
    var self=this;
    self.autorun(function () {

        self.subscribe('metasComprension', Session.get('currentPlan')._id);


    });
});
Template.MetasComprension.events({
    'click .btnAgregarMeta':function(event,template) {
        var meta=template.find('#inputMeta').value;
        currentPlan=Session.get('currentPlan');

        var listaMetasCompleto=PlanAnual.findOne({_id: currentPlan._id}).metaComprension;
        if(meta!="") {

            if(countCursor(listaMetasCompleto)<7) {
                if(findOrder(listaMetasCompleto)==0) {
//                    Meteor.call('iniciarlizarContenidoMetas', Session.get('currentPlan')._id, meta, orden);
                    Meteor.call('updatePushMetaComprension', Session.get('currentPlan')._id, meta, '2');
                    $('#inputMeta').val("");


                }
                else {
                    sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">EL número de meta ya ha sido seleccionado</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});


                }
            }

            else{
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten mas de 7 metas de comprensión</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
                $('#inputHiloConductor').val("");


            }

        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});



        }
        return false;
    },
    'click .eliminarMeta':function () {

        //Meteor.call('eliminarContenidoMetas',this._id);
        Meteor.call('updatePullMetaComprension',Session.get('currentPlan')._id,this.texto,this.orden)
        if(this.orden<=6){
            var listaMetasCompleto=PlanAnual.findOne({_id: currentPlan._id}).metaComprension;

        }
    },
    'click .editarMeta':function () {
        Session.set('nav-actividades-comprension','open');
        Session.setPersistent('currentMeta',this);
    }

});
Template.MetasComprension.helpers({
    meta:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var listMetas = PlanAnual.findOne({_id: currentPlan._id}).metaComprension;

            return listMetas;
        }
    }
});
function findOrder(col,value){
    var validar=0;
    col.forEach(function (doc) {
      if(doc.orden==value)
          validar=1;
    });
    return validar;
}
function verAnterior(col){
    cont=1;
    for(var i=0;i<countCursor(col);i++){

        var texto=col[i].texto;
        Meteor.call('updatePullMetaComprension',Session.get('currentPlan')._id,col[i].texto,col[i].orden);
        Meteor.call('updatePushMetaComprension',Session.get('currentPlan')._id,texto,i+1);




            }

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}
