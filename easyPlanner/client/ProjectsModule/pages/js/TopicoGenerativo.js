Template.TopicoGenerativo.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.TopicoGenerativo.events({
    'input #inputTopico':function (event,template) {
        var titulo=template.find('#inputTopico').value;
        if(!titulo=='') {
            Meteor.call('updateTopicoGenerativo', Session.get('currentPlan')._id, titulo);
        }

    },


});
Template.TopicoGenerativo.helpers({
    topico:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var topico = PlanAnual.findOne({_id: currentPlan._id}).topicoGenerativo;
            return topico;
        }
    }
});
