Template.DesempenosComprension.onCreated(function(){
    var self=this;
    self.autorun(function () {


    });
});
Template.DesempenosComprension.events({
    'click .btnActividades':function(event,template) {
        Session.set('nav-actividades','open');
        return false;
    },
    'click .eliminarActividad':function () {
//       MetasComprension.findOne({texto:})

        Meteor.call('eliminarContenidoMetas',this._id);
        return false;

    },
    'click .editarActividad':function () {
        Session.set('nav-actividades-comprension','open');
        Session.setPersistent('currentMeta',this);
       // Session.set('actividadEdit',this.texto);
       // Session.set('actividadDetalleEdit',this.detalle);

        return false;

    }

});
Template.DesempenosComprension.helpers({
    actividadesList:function(){
        currentContentidoConceptual=Session.get('currentPlan');
        if(!currentContentidoConceptual=='') {
            var listActividades = MetasComprension.find({idPlanAnual: currentContentidoConceptual._id});
            return listActividades;
        }
    }
});