

Template.ActividadComprension.onCreated(function () {
    var self=this;
    self.autorun(function () {


    });
});

Template.ActividadComprension.helpers({
    actividades:function () {
        return Session.get('currentMeta');
    }

});

Template.ActividadComprension.events({

'click .close-compresion':function () {
    Session.set('nav-actividades-comprension','');
},
'click .btn-guardarActividad':function () {
    Session.set('nav-actividades-comprension','');
    sAlert.info('<i class="fa fa-info-circle"></i> <br>Los Datos Han sido Guardados con exito!', {effect: 'bouncyflip', position: 'top-right', timeout: 4000, onRouteClose: false, stack: false,html:true, offset: '50px'});
return false;
}



});
