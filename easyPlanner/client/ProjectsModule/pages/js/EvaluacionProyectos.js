
Template.EvaluacionProyectos.helpers({
        indicadores:()=>{
        currentUnityCIE=Session.get('currentMeta');
if(!currentUnityCIE=='') {
    return MetasComprension.findOne({_id: currentUnityCIE._id}).evaluacionContinua;
}
}
});
Template.EvaluacionProyectos.events({
    'click .btnAgregarIndicador':function () {
        Session.set('nav-indicadores', 'open');



        return false;
    },
    'click .eliminarIndicador':function () {
        Meteor.call('updatePullEvaluacion',Session.get('currentMeta')._id,this.texto);

    }

});

