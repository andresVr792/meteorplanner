Template.HilosConductores.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.HilosConductores.events({
    'click .btnAgregarHilo':function(event,template) {
        var hiloConductor=template.find('#inputHiloConductor').value;
        currentPlan=Session.get('currentPlan');
        var listHilosConductores = PlanAnual.findOne({_id: currentPlan._id}).hiloConductor;
        if(hiloConductor!="") {
            if(countCursor(listHilosConductores)<4) {
                Meteor.call('updatePushHiloConductor', Session.get('currentPlan')._id, hiloConductor);
                $('#inputHiloConductor').val("");
            }
            else{
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten mas de 4 hilos conductores</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
                $('#inputHiloConductor').val("");


            }
        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});



        }
        return false;
    },
    'click .eliminarHiloCoductor':function () {
        Meteor.call('updatePullHiloConductor',Session.get('currentPlan')._id,this.texto);

    }

});
Template.HilosConductores.helpers({
    hilosConductores:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var listHilosConductores = PlanAnual.findOne({_id: currentPlan._id}).hiloConductor;
            return listHilosConductores;
        }
    }
});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}