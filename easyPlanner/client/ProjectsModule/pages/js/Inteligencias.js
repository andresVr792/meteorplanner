

Template.Inteligencias.onCreated(function () {
    var self=this;
    self.autorun(function () {

        self.subscribe('inteligencias');

    });
});

Template.Inteligencias.helpers({

    inteligencias: function () {
        return Inteligencias.find({});
    },
    actividades:function () {

        return MetasComprension.findOne({_id:Session.get('currentMeta')._id});
    }
});

Template.Inteligencias.events({


    'change form#InteligenciasFrm #inteligenciaSelect': function(event,template){
        var inteligenciasList=   MetasComprension.findOne({_id:Session.get('currentMeta')._id}).inteligencias;
        var inteligencia=template.find('#inteligenciaSelect').value;
        inteligencias =  MetasComprension.findOne({_id:Session.get('currentMeta')._id}).inteligencias;

        if (!findRepetitions(inteligenciasList, inteligencia)) {
            Meteor.call('updatePushInteligencias', Session.get('currentMeta')._id, template.find('#inteligenciaSelect').value);
        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">La inteligencia ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }


        $('#inteligenciaSelect').val($('#inteligenciaSelect > option:first').val());
    },
    'click .eliminarInteligencia':function () {

        Meteor.call('updatePullInteligencias',Session.get('currentMeta')._id,this.texto)
    }


});
function findRepetitions(collection,value){
    var a;
    collection.forEach(function (post) {

        if(post.texto==value)
            a=true;
    });
    return a;
}