Template.Tipo.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('tipoActividad');
        opEditarPlan=Session.get('op-editarPlan');
        /*if(!opEditarPlan=='open') {
         Meteor.call('updateAreaConocimiento', Session.get('currentPlan')._id, Session.get('logedUser').profile.areaConocimiento);
         var area = Areas.findOne({value: Session.get('logedUser').profile.areaConocimiento});
         Session.setPersistent('areaObjTmp', area);
         }*/
    });
});
Template.Tipo.helpers({
        tipoActividad: ()=>{
        return TipoActividad.find({});
},
tipoMeta:function(){

    currentMeta=Session.get('currentMeta');
    if(!currentMeta=='') {
        var meta = MetasComprension.findOne({_id: currentMeta._id});

        return MetasComprension.findOne({_id: currentMeta._id});


    }
},
logedUserMeta:function () {
    return "SELECCCIONAR TIPO DE ACTIVIDAD"

},
modoEdicionAL:()=>{
    currentMeta=Session.get('currentMeta');
    if(!currentMeta=='') {
        var meta = MetasComprension.findOne({_id: currentMeta._id});

        return !meta.tipo;
    }
}
});
Template.Tipo.events({
    'change form#tipoActividad #tipoActividadslc': function(event,template){
        var tipoActividad=template.find('#tipoActividadslc').value;

        Meteor.call('actualizarTipo',Session.get('currentMeta')._id,tipoActividad);
        $('#tipoActividadslc').val($('#tipoActividadslc > option:first').val());
        template.find('#metaL').value=tipoActividad;
    }
});
