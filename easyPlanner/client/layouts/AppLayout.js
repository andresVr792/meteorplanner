Template.AppLayout.onCreated(function(){
    var self=this;
    self.autorun(function () {
        Session.setPersistent('op-agregarPlan','');
        Session.setPersistent('op-editarPlan','');
        Session.setPersistent('nav-nuevoPlanAnual','');
        Session.setPersistent('gradoTmp','');
        Session.setPersistent('dimensionITmp','');
        Session.setPersistent('areaTmp','');
        Session.setPersistent('currentPlan','');
        Session.setPersistent('areaObjTmp','');
        Session.setPersistent('subnivelSingle','');
        Session.setPersistent('subnivelTmp','');
        Session.setPersistent('asignaturaTmp','');
        Session.setPersistent('nav-editarPlan','');
        Session.setPersistent('op-nuevoPlan','');
        Session.setPersistent('currentUnity','');
        Session.setPersistent('currentPlanId','');
        Session.setPersistent('currentPlanNombre','');
        Session.setPersistent('asignaturaTmpText','');
        Session.setPersistent('tipoEvaluacion','');
        Session.set('compartir','');
        Session.set('nav-actividades-edit','');

    });
});
