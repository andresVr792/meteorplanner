Template.ControlActividades.rendered=function () {
    if(!this._rendered){
        this._rendered=true;
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','');
        Session.set('plan','anual');
        Session.setPersistent('unidad','')
        Session.set('nav-configuracion','');
        Session.set('nav-objetivoIntegrador', '');
        Session.set('nav-objetivoEspecificos', '');
        Session.set('nav-destrezas', '');
        Session.set('nav-indicadores', '');
        Session.set('nav-nuevaUnidad', '');
        Session.set('subnivelTmp','');
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','open');
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('aaa',null);
        Session.set('gradoTmp',null);
        Session.set('dimensionITmp',null);
        Session.set('areaTmp',null);
        Session.set('currentPlan',' ');
        Session.set('areaObjTmp',null);
        Session.set('subnivelSingle',null);
        Session.set('subnivelTmp',null);
        Session.set('asignaturaTmp',null);
        Session.set('nav-editarPlan',null);
        Session.set('op-nuevoPlan',null);
        Session.set('currentUnity',null);
        Session.set('currentPlanId',null);
        Session.set('currentPlanNombre',null);
        Session.set('asignaturaTmpText',null);
        Session.set('tipoEvaluacion',null);
        Session.set('sec-contenidosConceptules',null);
        $('#configuracion').hide();
        Session.set('nav-configuracion',null);
        Session.set('quimestreTmp',null);
        Session.set('anioTmp',null);
        Session.set('compartir',null);
        Session.set('op-activeTask', null);

        $('#configuracion').hide();
        if(Session.get('plan')=="anual")
        {
            this.subscribe('compartidoAnual', Meteor.userId(), Session.get('logedUser'));
        }
        if(Session.get('plan')=="elemental"){
            this.subscribe('compartidoElemental', Meteor.userId(), Session.get('logedUser'));

        }
    }
}
Template.ControlActividades.helpers({
    ocultarNuevoPlanFrm:function () {
        return Session.get('op-activeTask')==='active';
    },
    getCurrentPlan:function () {
        return Session.get('currentPlan').nombre;
    },
    editarPlan:function () {
        return Session.get('op-editarPlan')===''||Session.get('op-editarPlan')==='active';

    },
    agregarPlan:function () {
        return Session.get('op-agregarPlan')===''||Session.get('op-agregarPlan')==='active';

    },
    modoCompartido:function () {
        return Session.get('compartir')!='compartidoAnual';
    }
});
Template.ControlActividades.onCreated(function () {
    this.autorun(()=>{

        this.subscribe('planAnual',Meteor.userId());
}) ;
});

Template.ControlActividades.events({
    'click .op-AgregarPlan':function (){
        Session.set('op-agregarPlan','open');
        Session.set('op-editarPlan','');
    },
    'click .op-EditarPlan':function(){
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','open');
    },
    'click .configuracionPlan':function(){
        Session.set('nav-configuracion','open');
    }
});
