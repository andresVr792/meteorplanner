Template.GenericTask.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('unidadesPlan',Session.get('currentPlan'));
        if(Session.get('currentPlan')!=null)
        self.subscribe('contenidosConceptualesUnidadFilter',Session.get('currentPlan'));

    });
});




Template.GenericTask.helpers({
    unidades: function () {
        return Unidad.find();
    },
    contenidos:function (idunidad) {
     return  ContenidosConceptualesUnidad.find({'idUnidad':idunidad});
    },
    pendientes:function (idunidad) {
        return ContenidosConceptualesUnidad.find({'idUnidad':idunidad,'estado':'text-red'}).count();
    },
    comentarios:function (idContenido) {

        return countCursor(ContenidosConceptualesUnidad.findOne({'_id':idContenido}).comentario);
    },
    adjuntos:function (idContenido) {
        return countCursor(ContenidosConceptualesUnidad.findOne({'_id':idContenido}).adjunto);
    }

});
Template.GenericTask.events({
    'click .btn-AccionTask':function (event,template) {

        Session.set('nav-ventanaAcciones', 'open');
        Session.setPersistent('currentContenidoConceptual',this._id);

        },
    'click .comentAction':function(events, template) {

        Session.setPersistent('currentContenidoConceptual',this._id);
        Session.set('nav-ventanaComentario','open');
    },
    'click .adjuntAction':function(events, template) {

        Session.setPersistent('currentContenidoConceptual',this._id);
        Session.set('nav-ventanaAdjunto','open');
    },



});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}