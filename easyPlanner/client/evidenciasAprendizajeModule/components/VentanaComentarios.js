import moment from 'moment';
Template.VentanaAcciones.onCreated(function () {
    this.autorun(() => {


        this.subscribe('planAnual', Meteor.userId());



})
    ;
});

Template.VentanaComentario.helpers({

    contenidoConceptual: function()  {

        return ContenidosConceptualesUnidad.findOne({"_id":Session.get('currentContenidoConceptual')}).comentario;

    },

})
;
Template.VentanaComentario.rendered = function () {
    $("#slcContenidosPlanAnual").select2({
        placeholder: "Selecciona/busca el contenido",
        language: "es",
        tags:true

    });
}

Template.VentanaComentario.events({

        'change form#AccionestaskFrm #slcEstadoContenidosConceptuales': function (event, template) {


        },
        'click .btn-AgregarAccionTask':(event,template) =>{
        var estadoContenido=template.find('#slcEstadoContenidosConceptuales').value;
var comentario=template.find('#txtAreaComentarioTask').value;

estadoContenido=estadoContenido.split('.');
if(estadoContenido!='')
    Meteor.call('actualizarComentarioPush',Session.get('currentContenidoConceptual'),comentario,'De: '+ContenidosConceptualesUnidad.findOne(Session.get('currentContenidoConceptual')).textoEstado+' a: '+ estadoContenido[0]);

Meteor.call('actualizarEstado',Session.get('currentContenidoConceptual'),estadoContenido[1]);
Meteor.call('actualizarTextoEstado',Session.get('currentContenidoConceptual'),estadoContenido[0]);
Session.set('nav-ventanaAcciones', '');

return false;
},
'click .close-coment':(event, template) =>{


    Session.set('nav-ventanaComentario', '');

}

});