import moment from 'moment';
Template.PlanAnualCombo.onCreated(function () {
    this.autorun(() => {


        this.subscribe('planAnual', Meteor.userId());



})
    ;
});

Template.PlanAnualCombo.helpers({
    ocultarNuevoPlanFrm: function () {
        return Session.get('nav-nuevoPlanAnual') === '';
    },
    compartirActivado:function () {
        return Session.get('compartir')!=null;
    },
    userPlans: function()  {

            return PlanAnual.find({"autor.idDocente":Meteor.userId()});

    },
    dateFormat:function () {
        return moment(this.fechaCreacion).format('MM/D/YYYY | HH:MM');
    }
})
;
Template.PlanAnualCombo.rendered = function () {
    $("#slcContenidosPlanAnual").select2({
        placeholder: "Selecciona/busca el contenido",
        language: "es",
        tags:true

    });
}

Template.PlanAnualCombo.events({

    'click .btn-SeleccionarPlan': function (event, template) {
        idTmp=template.find('#slcContenidosPlanAnual').value
        plan=PlanAnual.findOne({'_id':idTmp});

        Session.set('op-activeTask', 'active');

        Session.setPersistent('currentPlan', plan);
        Session.setPersistent('currentPlanNombre', plan.nombre);
        Session.setPersistent('currentPlanId', plan._id);


        return false;


    }

});