import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import Images from '../../../lib/images.collection.js';
import './VentanaAcciones.html';

import moment from 'moment';
Template.VentanaAcciones.onCreated(function () {
    this.autorun(() => {


        this.subscribe('planAnual', Meteor.userId());
    this.currentUpload = new ReactiveVar(false);


})
    ;
});

Template.VentanaAcciones.helpers({
    ocultarNuevoPlanFrm: function () {
        return Session.get('nav-nuevoPlanAnual') === '';
    },
    compartirActivado:function () {
        return Session.get('compartir')!=null;
    },
    userPlans: function()  {

            return PlanAnual.find({"autor.idDocente":Meteor.userId()});

    },
    dateFormat:function () {
        return moment(this.fechaCreacion).format('MM/D/YYYY | HH:MM');
    },
    currentUpload: function () {
        return Template.instance().currentUpload.get();
    }
})
;
Template.VentanaAcciones.rendered = function () {
    $("#slcContenidosPlanAnual").select2({
        placeholder: "Selecciona/busca el contenido",
        language: "es",
        tags:true

    });
}

Template.VentanaAcciones.events({

    'change form#AccionestaskFrm #slcEstadoContenidosConceptuales': function (event, template) {


    },
        'change #fileInput': function (e, template) {
            if (e.currentTarget.files && e.currentTarget.files[0]) {
                // We upload only one file, in case
                // there was multiple files selected
                var file = e.currentTarget.files[0];
                if (file) {
                    var uploadInstance = Images.insert({
                        file: file,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, false);

                    uploadInstance.on('start', function() {
                        template.currentUpload.set(this);
                    });

                    uploadInstance.on('end', function(error, fileObj) {
                        if (error) {
                            // window.alert('Error en la carga del archivo: ' + error.reason);
                        } else {
                            var currentAdjunto={
                                _id:fileObj._id,
                                name:fileObj.name,
                                extension:fileObj.extensionWithDot,
                                link:'/resources/'+fileObj._id+fileObj.extensionWithDot
                            }
                            Session.setPersistent('currentAdjunto',currentAdjunto);
                            //window.alert('El archivo "' + fileObj.name + '" cargado exitosamente');
                        }
                        //
                    });

                    uploadInstance.start();
                }
            }
        },
    'click .btn-AgregarAccionTask':(event,template) =>{
        var estadoContenido=template.find('#slcEstadoContenidosConceptuales').value;
        var comentario=template.find('#txtAreaComentarioTask').value;

        estadoContenido=estadoContenido.split('.');
        if(estadoContenido.length>0)
            Meteor.call('actualizarComentarioPush',Session.get('currentContenidoConceptual'),comentario,'De: '+ContenidosConceptualesUnidad.findOne(Session.get('currentContenidoConceptual')).textoEstado+' a: '+ estadoContenido[0]);
        Meteor.call('actualizarEstado',Session.get('currentContenidoConceptual'),estadoContenido[1]);
        Meteor.call('actualizarTextoEstado',Session.get('currentContenidoConceptual'),estadoContenido[0]);
        if(Session.get('currentAdjunto')!=null)
            Meteor.call('actualizarAdjuntoPush',Session.get('currentContenidoConceptual'),Session.get('currentAdjunto'),'De: '+ContenidosConceptualesUnidad.findOne(Session.get('currentContenidoConceptual')).textoEstado+' a: '+ estadoContenido[0]);
        Session.set('nav-ventanaAcciones', '');
        template.currentUpload.set(false);
        return false;
},
    'click .close-actividad':(event, template) =>{


        Session.set('nav-ventanaAcciones', '');
    template.currentUpload.set(false);
},


});