Template.MainNav.events({
    'click .login-toggle':()=>{
        Session.set('nav-toggle','open');
    },
    'click .logout':()=>{
        AccountsTemplates.logout();

    }
});
Template.MainNav.helpers({
    logedUser:function () {
        return Session.get('logedUser').profile.firstName;
    }
});