Template.ObjetivosLeccionCopperativa.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.ObjetivosLeccionCopperativa.events({
    'input #inputObjetivoAcademico':function (event,template) {
        var objetivoAcademico=template.find('#inputObjetivoAcademico').value;
        if(!objetivoAcademico=='') {
            Meteor.call('updateObjetivoAcademico', Session.get('currentPlan')._id, objetivoAcademico);
        }

    },
    'input #inputHabilidadSocial':function (event,template) {
        var objetivo=template.find('#inputHabilidadSocial').value;
        if(!objetivo=='') {
            Meteor.call('updateObjetivoHabilidadSocial', Session.get('currentPlan')._id, objetivo);
        }

    }


});
Template.ObjetivosLeccionCopperativa.helpers({
    academico:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var objetivoAcademico = PlanAnual.findOne({_id: currentPlan._id}).objetivoAcademico;
            return objetivoAcademico;
        }
    },
    habilidad:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var objetivoHabilidadSocial = PlanAnual.findOne({_id: currentPlan._id}).objetivoHabilidadSocial;
            return objetivoHabilidadSocial;
        }
    }
});
