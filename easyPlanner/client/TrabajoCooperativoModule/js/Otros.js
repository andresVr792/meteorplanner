Template.Otros.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.Otros.events({
    'click .btnAgregarOtros':function(event,template) {
        var otros=template.find('#inputOtros').value;
        currentPlan=Session.get('currentPlan');
        var lstRoles = PlanAnual.findOne({_id: currentPlan._id}).otros;
        if(otros!="") {
            Meteor.call('updatePushOtros', Session.get('currentPlan')._id, otros);
            $('#inputOtros').val("");

        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});



        }
        return false;
    },
    'click .eliminarHiloCoductor':function () {
        Meteor.call('updatePullOtros',Session.get('currentPlan')._id,this.texto);

    }

});
Template.Otros.helpers({
    otros:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var lstOtros = PlanAnual.findOne({_id: currentPlan._id}).otros;
            return lstOtros;
        }
    }
});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}