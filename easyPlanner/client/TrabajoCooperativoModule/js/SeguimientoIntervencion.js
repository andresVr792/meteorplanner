Template.SeguimientoIntervencion.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.SeguimientoIntervencion.events({
    'change form#frmProcedimiento #slcProcedimiento':function (event,template) {
        var procedimiento=template.find('#slcProcedimiento').value;
        if(!procedimiento=='') {
            Meteor.call('updateProcedimientoObservacion', Session.get('currentPlan')._id, procedimiento);
        }
        $('#slcProcedimiento').val($('#slcProcedimiento > option:first').val());
    },
    'input #inputIntervencion':function (event,template) {
        var intervencion=template.find('#inputIntervencion').value;
        if(!intervencion=='') {
            Meteor.call('updateIntervencionParaAyudarSobreTarea', Session.get('currentPlan')._id, intervencion);
        }

    },

    'input #inputIntervencionTrabajoEquipo':function (event,template) {
        var intervencionTrabajo=template.find('#inputIntervencionTrabajoEquipo').value;
        if(!intervencionTrabajo=='') {
            Meteor.call('updateIntervencionTrabajoEquipo', Session.get('currentPlan')._id, intervencionTrabajo);
        }

    },




});
Template.SeguimientoIntervencion.helpers({
    observacion:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var observacion = PlanAnual.findOne({_id: currentPlan._id}).procedimientoObservacion;
            return observacion;
        }
    },
    intervencion:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var intervencion = PlanAnual.findOne({_id: currentPlan._id}).intervencionParaAyudarSobreTarea;
            return intervencion;
        }
    },
    intervencionTrabajoEquipo:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var intervencionTrabajo = PlanAnual.findOne({_id: currentPlan._id}).intervencionTrabajoEquipo;
            return intervencionTrabajo;
        }
    }
});
