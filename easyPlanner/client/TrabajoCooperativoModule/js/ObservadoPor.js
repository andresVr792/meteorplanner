import '../../../public/plugins/select2/i18n/es';
import '../../../public/plugins/select2/select2.full.min.js';

Template.ObservadorPor.onCreated(function(){
    var self=this;
    self.autorun(function () {
    });
});

Template.ObservadorPor.rendered = function () {
    $("#slcObservadoPor").select2({
        placeholder: "Selecciona/busca",
        language: "es",
        tags:true,
        allowClear:true

    });
}
Template.ObservadorPor.helpers({
    plan:function () {
        currentPlanP=Session.get('currentPlan');
        if(!currentPlanP=='') {
            var a = PlanAnual.findOne({_id: currentPlanP._id});
            return a;
        }
    }


});


Template.ObservadorPor.events({
    'change form#observadoPor #slcObservadoPor': function(event,template){
        var observadoPorList=   PlanAnual.findOne({_id:Session.get('currentPlan')._id}).observadorPor;
        var observadoPor=template.find('#slcObservadoPor').value;
        observadosPor = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).observadorPor;
        porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
        if (countCursor(observadosPor)==0) {
            Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 5);
            Meteor.call('updatePushObservadorPor', Session.get('currentPlan')._id, template.find('#slcObservadoPor').value);

        }
        else {
            if (!findRepetitions(observadoPorList, observadoPor)) {
                Meteor.call('updatePushObservadorPor', Session.get('currentPlan')._id, template.find('#slcObservadoPor').value);
            }
            else {
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El contenido ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                    effect: 'bouncyflip',
                    position: 'top-right',
                    timeout: 3000,
                    onRouteClose: false,
                    stack: false,
                    html: true,
                    offset: '50px'
                });

            }
        }

        $('#slcMateriales').val($('#slcMateriales > option:first').val());
    },
    'click .eliminarObservadoPor':function (event, template) {
        Meteor.call('updatePullObservadorPor',Session.get('currentPlan')._id,this.texto);
        porcentajePlanCompleto=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado;
        materiales=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).observadorPor;
        if(countCursor(materiales)<=1){
            if(PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado>10)
                Meteor.call('updatePlanCompletado',Session.get('currentPlan')._id,porcentajePlanCompleto-5);
        }
    }

});
function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}