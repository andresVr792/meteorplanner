import '../../../public/plugins/select2/i18n/es';
import '../../../public/plugins/select2/select2.full.min.js';

Template.Materiales.onCreated(function(){
    var self=this;
    self.autorun(function () {
    });
});

Template.Materiales.rendered = function () {
    $("#slcMateriales").select2({
        placeholder: "Selecciona/busca material",
        language: "es",
        tags:true,
        allowClear:true

    });
}
Template.Materiales.helpers({
    plan:function () {
        currentPlanP=Session.get('currentPlan');
        if(!currentPlanP=='') {
            var a = PlanAnual.findOne({_id: currentPlanP._id});
            return a;
        }
    }


});


Template.Materiales.events({
    'change form#materiales #slcMateriales': function(event,template){
        var materialesList=   PlanAnual.findOne({_id:Session.get('currentPlan')._id}).material;
        var material=template.find('#slcMateriales').value;
        materiales = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).material;
        porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
        if (countCursor(materiales)==0) {
            Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 5);
            Meteor.call('updatePushMaterial', Session.get('currentPlan')._id, template.find('#slcMateriales').value);

        }
        else {
            if (!findRepetitions(materialesList, material)) {
                Meteor.call('updatePushMaterial', Session.get('currentPlan')._id, template.find('#slcMateriales').value);
            }
            else {
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El material ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                    effect: 'bouncyflip',
                    position: 'top-right',
                    timeout: 3000,
                    onRouteClose: false,
                    stack: false,
                    html: true,
                    offset: '50px'
                });

            }
        }

        $('#slcMateriales').val($('#slcMateriales > option:first').val());
    },
    'click .eliminarMaterial':function (event, template) {
        Meteor.call('updatePullMaterial',Session.get('currentPlan')._id,this.texto);
        porcentajePlanCompleto=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado;
        materiales=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).material;
        if(countCursor(materiales)<=1){
            if(PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado>10)
                Meteor.call('updatePlanCompletado',Session.get('currentPlan')._id,porcentajePlanCompleto-5);
        }
    }

});
function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}