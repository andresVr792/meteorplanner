Template.Roles.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.Roles.events({
    'click .btnAgregarRol':function(event,template) {
        var rol=template.find('#inputRol').value;
        currentPlan=Session.get('currentPlan');
        var lstRoles = PlanAnual.findOne({_id: currentPlan._id}).roles;
        if(rol!="") {
                Meteor.call('updatePushRoles', Session.get('currentPlan')._id, rol);
                $('#inputRol').val("");

        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});



        }
        return false;
    },
    'click .eliminarHiloCoductor':function () {
        Meteor.call('updatePullRoles',Session.get('currentPlan')._id,this.texto);

    }

});
Template.Roles.helpers({
    roles:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var lstRoles = PlanAnual.findOne({_id: currentPlan._id}).roles;
            return lstRoles;
        }
    }
});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}