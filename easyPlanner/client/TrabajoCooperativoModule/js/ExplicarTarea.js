Template.Explicartarea.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.Explicartarea.events({
    'input #inputTarea':function (event,template) {
        var tamanio=template.find('#inputTarea').value;
        if(!tamanio=='') {
            Meteor.call('updateTarea', Session.get('currentPlan')._id, tamanio);
        }

    },
    'input #inputCriterioExito':function (event,template) {
        var objetivo=template.find('#inputCriterioExito').value;
        if(!objetivo=='') {
            Meteor.call('updateCriterioParaExito', Session.get('currentPlan')._id, objetivo);
        }

    },

    'input #inputInterdependeciaPositiva':function (event,template) {
        var distribucion=template.find('#inputInterdependeciaPositiva').value;
        if(!distribucion=='') {
            Meteor.call('updateInterdependeciaPositiva', Session.get('currentPlan')._id, distribucion);
        }

    },
    'input #inputResponsabilidadIndividual':function (event,template) {
        var distribucion=template.find('#inputResponsabilidadIndividual').value;
        if(!distribucion=='') {
            Meteor.call('updateResponsabilidadIndividual', Session.get('currentPlan')._id, distribucion);
        }

    },
    'input #inputCooperacionIntergrupos':function (event,template) {
        var distribucion=template.find('#inputCooperacionIntergrupos').value;
        if(!distribucion=='') {
            Meteor.call('updateCooperacionIntergrupos', Session.get('currentPlan')._id, distribucion);
        }

    },
    'input #inputComportamientoEsperado':function (event,template) {
        var distribucion=template.find('#inputComportamientoEsperado').value;
        if(!distribucion=='') {
            Meteor.call('updateComportamientosEsperados', Session.get('currentPlan')._id, distribucion);
        }

    },



});
Template.Explicartarea.helpers({
    tarea:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var tamanioGrupo = PlanAnual.findOne({_id: currentPlan._id}).tarea;
            return tamanioGrupo;
        }
    },
    criterioExito:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var metodoAsignarAlumnos = PlanAnual.findOne({_id: currentPlan._id}).criterioParaExito;
            return metodoAsignarAlumnos;
        }
    },
    interdependeciaPositiva:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var distribucion = PlanAnual.findOne({_id: currentPlan._id}).interdependeciaPositiva;
            return distribucion;
        }
    },
    responsabilidadIndividual:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var distribucion = PlanAnual.findOne({_id: currentPlan._id}).responsabilidadIndividual;
            return distribucion;
        }
    },
    cooperacionIntergrupos:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var distribucion = PlanAnual.findOne({_id: currentPlan._id}).cooperacionIntergrupos;
            return distribucion;
        }
    },
    comportamientoEsperado:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var distribucion = PlanAnual.findOne({_id: currentPlan._id}).comportamientosEsperados;
            return distribucion;
        }
    }
});
