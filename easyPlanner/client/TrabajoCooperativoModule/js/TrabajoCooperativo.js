Template.TrabajoCooperativo.rendered=function () {
    if(!this._rendered){
        this._rendered=true;
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','');
        Session.set('plan','trabajoCooperativo');
        Session.setPersistent('unidad','')
        Session.set('nav-configuracion','');
        Session.set('nav-objetivoIntegrador', '');
        Session.set('nav-objetivoEspecificos', '');
        Session.set('nav-destrezas', '');
        Session.set('nav-indicadores', '');
        Session.set('nav-nuevaUnidad', '');
        Session.set('subnivelTmp','');
        $('#configuracion').hide();

        if(Session.get('plan')=="trabajoCooperativo"){
            this.subscribe('compartidoTrabajoCooperativo', Meteor.userId(), Session.get('logedUser'));

        }
    }
}
Template.TrabajoCooperativo.helpers({
    ocultarNuevoPlanFrm:function () {
        return Session.get('nav-nuevoPlanAnual')==='';
    },
    getCurrentPlan:function () {
        return Session.get('currentPlan').nombre;
    },
    editarPlan:function () {
        return Session.get('op-editarPlan')===''||Session.get('op-editarPlan')==='active';

    },
    agregarPlan:function () {
        return Session.get('op-agregarPlan')===''||Session.get('op-agregarPlan')==='active';

    },
    modoCompartido:function () {
        return Session.get('compartir')!='compartidoTrabajoCooperativo';
    }
});
Template.TrabajoCooperativo.onCreated(function () {
    this.autorun(()=>{

        this.subscribe('planAnual',Meteor.userId());
}) ;
});

Template.TrabajoCooperativo.events({
    'click .op-AgregarPlan':function (){
        Session.set('op-agregarPlan','open');
        Session.set('op-editarPlan','');
    },
    'click .op-EditarPlan':function(){
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','open');
    },
    'click .configuracionPlan':function(){
        Session.set('nav-configuracion','open');
    }
});
