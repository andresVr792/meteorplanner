Template.DecisionesPreinstruccionales.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.DecisionesPreinstruccionales.events({
    'input #inputTamanioGrupo':function (event,template) {
        var tamanio=template.find('#inputTamanioGrupo').value;
        if(!tamanio=='') {
            Meteor.call('updateTamanioGrupo', Session.get('currentPlan')._id, tamanio);
        }

    },
    'input #inputMetodo':function (event,template) {
        var objetivo=template.find('#inputMetodo').value;
        if(!objetivo=='') {
            Meteor.call('updateMetodoAsignarAlumnos', Session.get('currentPlan')._id, objetivo);
        }

    },

    'input #inputDistribucionAula':function (event,template) {
        var distribucion=template.find('#inputDistribucionAula').value;
        if(!distribucion=='') {
            Meteor.call('updateDistribucionAula', Session.get('currentPlan')._id, distribucion);
        }

    },



});
Template.DecisionesPreinstruccionales.helpers({
    tamanioGrupo:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var tamanioGrupo = PlanAnual.findOne({_id: currentPlan._id}).tamanioGrupo;
            return tamanioGrupo;
        }
    },
    metodo:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var metodoAsignarAlumnos = PlanAnual.findOne({_id: currentPlan._id}).metodoAsignarAlumnos;
            return metodoAsignarAlumnos;
        }
    },
    distribucion:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=='') {
            var distribucion = PlanAnual.findOne({_id: currentPlan._id}).distribucionAula;
            return distribucion;
        }
    },
});
