import moment from 'moment';

Template.Report.onCreated(function () {
    this.autorun(()=>{
     //   this.subscribe('planAnual', FlowRouter.getParam("user"));
    this.subscribe('allplanAnual');
    this.subscribe('reportData',Meteor.userId());

}) ;
});

Template.Report.helpers({
    reportes:function () {
    return ReportRoutes.find({});

  },
    dateFormat:function () {
        return moment(this.fechaCreacion).format('MM/D/YYYY | HH:MM');
    }
});
Template.Report.events({
    'click .eliminarReporte':function () {
        Meteor.call('deleteReporte',this._id);

    },
    'click .informacionReporte':function () {
        Session.set('nav-infoReportes','open');
    }

   /* 'click .generarReporte':function () {
        NProgress.start(); //this starts the progress bar


        Meteor.call('phantom',
            'http://localhost:3000/educador/reporteAnualParametros/'+Session.get('currentPlanId')+'/'+Meteor.userId(),
            Session.get('currentPlanId')+'anual',Meteor.userId(), Session.get('currentPlanId'),
            function(err, res){
                if(res){
                    //Do whatever you want with the result
                    NProgress.done(); //this completes the progress bar
                }});
        Meteor.call('phantom',
            'http://localhost:3000/educador/reporteMicroParametros/'+Session.get('currentPlanId')+'/'+Meteor.userId(),
            Session.get('currentPlanId')+'micro',Meteor.userId(), Session.get('currentPlanId'),
            (error, result)=> {
            console.log('DOne');
    });
        return false;
    },
    'click .generarReporteMicro':function () {
        Meteor.apply('phantom',
            ['http://localhost:3000/educador/ReportePlanPrimero/'+Session.get('currentPlanId')+'/'+Meteor.userId(),
            Session.get('currentPlanId')+'primero'],Meteor.userId(), Session.get('currentPlanId'),
            function(err, res){
                if(res){
                    //Do whatever you want with the result
                    NProgress.done(); //this completes the progress bar
                }});
/*        Meteor.call('sendEmail',
            'avrz237@gmail.com',
            'domotic.user@gmail.com',
            'Hello from Meteor!',
            'This is a test of Email.send.',
            'sample.pdf',
            '/Users/espe/Documents/meteorplanner/easyPlanner/public/sample.pdf');
        return false;
    },*/


});

