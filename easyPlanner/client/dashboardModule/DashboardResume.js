Template.DashboardResume.topGenresChartSubnivel=function () {
    return {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Subnivel Educarivo'
        },
        subtitle: {
            text: 'Porcentaje de profesores por subnivel Educativo'
        },
        xAxis: {
            type: 'category',
            text: 'Subnivel Educativo'
        },
        yAxis: {
            title: {
                text: 'Porcentaje de profesores'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Subnivel Educativo',
            colorByPoint: true,
            data: [{
                name: 'Preparatorio',
                y: 56.33,
                drilldown: 'Microsoft Internet Explorer'
            }, {
                name: 'EGB-Elemental',
                y: 24.03,
                drilldown: 'Chrome'
            }, {
                name: 'EBG-Media',
                y: 10.38,
                drilldown: 'Firefox'
            }, {
                name: 'EGB-Superior',
                y: 4.77,
                drilldown: 'Safari'
            }, {
                name: 'BGU',
                y: 1.11,
                drilldown: 'Opera'
            }, ]
        }],

    }
};

Template.DashboardResume.topGenresChartAreaConocimiento=function () {
    return {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Áreas de Conocimiento'
        },
        subtitle: {
            text: 'Porcentaje de profesores por Área de Conocimiento'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Áreas de Conocimiento',
            colorByPoint: true,
            data: [{
                name: 'CN',
                y: 56.33
            }, {
                name: 'CS',
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: 'EF',
                y: 10.38
            }, {
                name: 'EFL',
                y: 4.77
            }, {
                name: 'LL',
                y: 0.91
            }, {
                name: 'M',
                y: 0.2
            }]
        }]
    };
};
Template.DashboardResume.helpers({
        modoSeguroOption:()=>{
        return Session.get('modoSeguridad')==true;
}
});

Template.DashboardResume.events({
    'change .toggleModoSeguro':function (events,template) {

        var flag=false
        var x=$('#modoSeguro').prop('checked');

        if(x){
            Session.setPersistent('modoSeguridad',true);
        }
        else{
            Session.setPersistent('modoSeguridad',false);
        }
    }
});
