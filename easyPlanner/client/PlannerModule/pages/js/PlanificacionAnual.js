Template.PlanificacionAnual.rendered=function () {
    if(!this._rendered){
        this._rendered=true;
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','');
        Session.set('plan','anual');
        Session.setPersistent('unidad','')
        Session.set('nav-configuracion','');
        Session.set('nav-objetivoIntegrador', '');
        Session.set('nav-objetivoEspecificos', '');
        Session.set('nav-destrezas', '');
        Session.set('nav-indicadores', '');
        Session.set('nav-nuevaUnidad', '');
        Session.set('subnivelTmp','');
        $('#configuracion').hide();
        if(Session.get('plan')=="anual")
        {
            this.subscribe('compartidoAnual', Meteor.userId(), Session.get('logedUser'));
        }
        if(Session.get('plan')=="elemental"){
            this.subscribe('compartidoElemental', Meteor.userId(), Session.get('logedUser'));

        }
    }
}
Template.PlanificacionAnual.helpers({
    ocultarNuevoPlanFrm:function () {
        return Session.get('nav-nuevoPlanAnual')==='';
    },
    getCurrentPlan:function () {
        return Session.get('currentPlan').nombre;
    },
    editarPlan:function () {
        return Session.get('op-editarPlan')===''||Session.get('op-editarPlan')==='active';

    },
    agregarPlan:function () {
        return Session.get('op-agregarPlan')===''||Session.get('op-agregarPlan')==='active';

    },
    modoCompartido:function () {
        return Session.get('compartir')!='compartidoAnual';
    }
});
Template.PlanificacionAnual.onCreated(function () {
    this.autorun(()=>{

        this.subscribe('planAnual',Meteor.userId());
}) ;
});

Template.PlanificacionAnual.events({
    'click .op-AgregarPlan':function (){
         Session.set('op-agregarPlan','open');
        Session.set('op-editarPlan','');
    },
    'click .op-EditarPlan':function(){
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','open');
    },
    'click .configuracionPlan':function(){
        Session.set('nav-configuracion','open');
    }
});
