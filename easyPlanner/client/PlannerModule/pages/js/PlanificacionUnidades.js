Template.PlanificacionUnidadAnual.rendered=function () {
    if(!this._rendered){
        this._rendered=true;
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','');
        Session.setPersistent('unidad','active')
        Session.set('nav-configuracion','');
        Session.set('nav-rti','');

        $('#datosInformacion').hide();
        if(Session.get('plan')=="anual")
        {
            this.subscribe('compartidoAnual', Meteor.userId(), Session.get('logedUser'));
        }
        if(Session.get('plan')=="elemental"){
            this.subscribe('compartidoElemental', Meteor.userId(), Session.get('logedUser'));

        }

        }

}
Template.PlanificacionUnidadAnual.helpers({
    ocultarNuevoPlanFrm:function () {
        return Session.get('nav-nuevoPlanAnual')==='';
    },
    getCurrentPlan:function () {
        return Session.get('currentPlan').nombre;
    },
    editarPlan:function () {
        return Session.get('op-editarPlan')===''||Session.get('op-editarPlan')==='active';

    },
    agregarPlan:function () {
        return Session.get('op-agregarPlan')===''||Session.get('op-agregarPlan')==='active';

    },
    datosMicroPlan:function () {
        return Session.get('plan')=='elemental';
    },
    tipoPlan:function () {
        return Session.get('plan')=='anual';
    }
});
Template.PlanificacionUnidadAnual.onCreated(function () {
    this.autorun(()=>{

        this.subscribe('planAnual',Meteor.userId());
}) ;
});

Template.PlanificacionAnual.events({
    'click .op-AgregarPlan':function (){
        Session.set('op-agregarPlan','open');
        Session.set('op-editarPlan','');
    },
    'click .op-EditarPlan':function(){
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','open');
    },
    'click .configuracionPlan':function(){
        Session.set('nav-configuracion','open');
    }
});
