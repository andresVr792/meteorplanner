Template.TiempoSection.helpers({

    tiempoSemanas:function () {
        return SEMANAS_DE_CLASE;
    },
    imprevistos:function () {
        return EVALUACION_APRENSIZAJE_IMPREVISTOS;
    },
    cargaHorariaSemanal:function () {
        if(Session.get('op-editarPlan')=='active')
            return PlanAnual.findOne({_id: Session.get('currentPlan')._id}).cargaHorariaSemanal;
            return 0;


    },
    totalPeriodos:function () {
        if(Session.get('op-editarPlan')=='active')
            return PlanAnual.findOne({_id: Session.get('currentPlan')._id}).totalPeriodos;
            return 0;


    }

});
Template.TiempoSection.events({
    'input #inCargaHorariaSemanal':function (event,template) {
       var cargaHoraria=template.find('#inCargaHorariaSemanal').value;
       var totalHoras=cargaHoraria*SEMANAS_DE_CLASE
        template.find('#inTotalPeriodos').value=totalHoras;
        Meteor.call('updateTiempoHorasCargaHoraria',Session.get('currentPlan')._id,totalHoras,cargaHoraria);


    }
});
Template.TiempoSection.onCreated(function(){
    var self=this;
    self.autorun(function () {
        Meteor.call('updateTiempoSemanasClaseImprevistos',Session.get('currentPlan')._id,SEMANAS_DE_CLASE,EVALUACION_APRENSIZAJE_IMPREVISTOS);

    });
});
