
Template.Asignatura.onCreated(function(){
    var self=this;
    self.autorun(function (event,template) {
       // self.subscribe('asignaturas',Session.get('logedUser').profile.areaConocimiento);
        self.subscribe('todasAsignaturas');

    });
});
Template.Asignatura.helpers({
    asignaturas:function(){
      //  return Asignaturas.find({area:Session.get('logedUser').profile.areaConocimiento});

        return Asignaturas.find({area:Session.get('areaTmp')});

    },
    asignaturaPlan:function () {
        currentPlanA=Session.get('currentPlan');
        if(!currentPlanA=='') {

            if (Session.get('op-editarPlan') == 'active' || Session.get('op-nuevoPlan') == 'active') {
                var plan = PlanAnual.findOne({_id: currentPlanA._id});
                if (plan.asignatura == null) {
                    return "SELECCIONAR ASIGNATURA";
                }
                else {
                    var asignatura = Asignaturas.findOne({value: plan.asignatura});
                    Session.set("asignaturaTmp", asignatura);
                    //    Session.set("asignaturaTmpText", asignatura.text);
                    return plan.asignatura;

                }

            } else {
                return "SELECCIONAR ASIGNATURA";

            }
        }

    }


});
Template.Asignatura.events({
    'change form#asignaturaFrm #slcAsignatura':function (event,template) {
       var asignaturaTmp=template.find('#slcAsignatura').value;
        var asignatura=Asignaturas.findOne({value:asignaturaTmp});
        Session.set("asignaturaTmp",asignatura);
        Meteor.call('updateAsignatura',Session.get('currentPlan')._id,asignaturaTmp);
        $('#slcAsignatura').val($('#slcAsignatura > option:first').val());
    }
});