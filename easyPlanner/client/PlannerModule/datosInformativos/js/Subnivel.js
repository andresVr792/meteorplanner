Template.SubnivelEducativo.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('subnivel');



    });
});
Template.SubnivelEducativo.helpers({
        subnivel: ()=>{
        return Subnivel.find({});
},
subnivelUsuario:function () {
        currentplanSubnivel=Session.get('currentPlan');
        if(!currentplanSubnivel=='') {
            var subnivel = PlanAnual.findOne({_id: currentplanSubnivel._id}).subnivelEducativo;
            Session.setPersistent('subnivelSingle', subnivel);
            Session.setPersistent('subnivelTmp', Subnivel.findOne({value: subnivel}));
            return subnivel;
        }



},
modoedicion:function(){
    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var area = PlanAnual.findOne({_id: currentPlanAC._id});
        return !area.subnivelEducativo;

    }

}

,
subnivelNuevo:function(){
    return "SELECCCIONAR SUBNIVEL";
}

});
Template.SubnivelEducativo.events({
    'change form#subnivel #subnivelEducativo': function(event,template){
        var subnivelTmp=template.find('#subnivelEducativo').value;
        Session.setPersistent('subnivelSingle',subnivelTmp);
        Session.setPersistent('subnivelTmp',Subnivel.findOne({value:subnivelTmp}));
        Meteor.call('updateSubnivel',Session.get('currentPlan')._id,subnivelTmp);
        $('#subnivelEducativo').val($('#subnivelEducativo > option:first').val());
        Meteor.call('updateGradoCurso',Session.get('currentPlan')._id,'');
        template.find('#subnivelE').value=subnivelTmp;
    }
});