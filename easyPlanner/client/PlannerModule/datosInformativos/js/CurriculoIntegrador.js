
Template.CurriculoIntegrador.onCreated(function(){
    var self=this;
    self.autorun(function (event,template) {
        // self.subscribe('asignaturas',Session.get('logedUser').profile.areaConocimiento);
        self.subscribe('curriculoIntegrador');
        self.subscribe('ejes');
    });
});
Template.CurriculoIntegrador.helpers({
    ejesDesarrollo:function(){
        return EjesDesarrolloElemental.find();
    },
    ambitoDesarrollo:function () {
        return CurriculoIntegradorElemental.find({ejes:Session.get('EjeDesarrollo')});


    },
    curriculo:function () {
        planCurrent=Session.get('currentPlan');
        if(!planCurrent=='') {
            return PlanAnual.findOne({_id: Session.get('currentPlan')._id}).curriculoIntegrador;
        }
    }


});
Template.CurriculoIntegrador.events({
    'change form#curriculo #ejesDesarrollo':function (event,template) {
        var eje=template.find('#ejesDesarrollo').value;
             Session.set("EjeDesarrollo",eje);

         },
    'change form#curriculo #ambitosDesarrollo':function (event,template) {
        var ambito=template.find('#ambitosDesarrollo').value;
    },
    'click .btnCurriculo':function () {
        Session.set('nav-curriculoIntegrador','open');

        return false;
    },
    'click .eliminarCurriculo':function () {
        Meteor.call('updatePullCurriculoIntegrador',Session.get('currentPlan'),this.ejesDesarrolloAprendizaje,this.ambitosDesarrolloAprendizaje);
        return false;
    }

});