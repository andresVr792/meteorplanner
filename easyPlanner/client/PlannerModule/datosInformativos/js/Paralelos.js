
Template.Paralelos.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('paralelo');
    });
});
Template.Paralelos.helpers({
    paralelos:function(){
        return Paralelo.find({},{sort:{nombreParalelo:1}});
    },
    paralelosPlan:function () {
        currentPlanP=Session.get('currentPlan');
        if(!currentPlanP=='') {
            var a = PlanAnual.findOne({_id: currentPlanP._id});
            return a;
        }
    }


});


Template.Paralelos.events({
    'change form#paralelo #slcParalelo': function(event,template){
        var paralelosList=   PlanAnual.findOne({_id:Session.get('currentPlan')._id}).paralelos;
        var paralelo=template.find('#slcParalelo').value;
        paralelos = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).paralelos;
        porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
        if (countCursor(paralelos)==0) {
            Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 5);
            Meteor.call('updatePushParalelos', Session.get('currentPlan')._id, template.find('#slcParalelo').value);

        }
        else {
            if (!findRepetitions(paralelosList, paralelo)) {
                Meteor.call('updatePushParalelos', Session.get('currentPlan')._id, template.find('#slcParalelo').value);
            }
            else {
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El paralelo ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                    effect: 'bouncyflip',
                    position: 'top-right',
                    timeout: 3000,
                    onRouteClose: false,
                    stack: false,
                    html: true,
                    offset: '50px'
                });

            }
        }

        $('#slcParalelo').val($('#slcParalelo > option:first').val());
    },
    'click .eliminarParalelo':function (event, template) {
        Meteor.call('updatePullParalelos',Session.get('currentPlan')._id,this.nombre);
        porcentajePlanCompleto=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado;
        paralelos=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).paralelos;
            if(countCursor(paralelos)<=1){
                if(PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado>10)
                    Meteor.call('updatePlanCompletado',Session.get('currentPlan')._id,porcentajePlanCompleto-5);
            }
        }

});
function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.nombre==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}