Template.GradoCurso.onCreated(function(){
    var self=this;
    self.autorun(function () {

        self.subscribe('gradoCurso',Session.get('subnivelSingle'));
    });
});
Template.GradoCurso.helpers({
        gradoCurso: function(){
        return GradoCurso.find({});
        },
        gradoPlan:function () {
            currentPlanAC=Session.get('currentPlan');
            if(!currentPlanAC=='') {
                var plan = PlanAnual.findOne({_id: Session.get('currentPlan')._id});

                var area = PlanAnual.findOne({_id: currentPlanAC._id});

                Session.setPersistent('gradoTmp', area.gradoCurso);
                return area;
            }



        },
        gradoNuevo:function(){
            return "SELECCIONAR GRADO/CURSO";
        }
        ,
        modoedicion:function(){
            currentPlanAC=Session.get('currentPlan');
            if(!currentPlanAC=='') {
                var area = PlanAnual.findOne({_id: currentPlanAC._id});

                return !area.gradoCurso;

            }

}

});
Template.GradoCurso.events({
    'change form#gradoCurso #gradosCurso': function(event,template){
        var gradoTmp=template.find('#gradosCurso').value;
        Session.setPersistent('gradoTmp',gradoTmp);
        Meteor.call('updateGradoCurso',Session.get('currentPlan')._id,gradoTmp);
        $('#gradosCurso').val($('#gradosCurso > option:first').val());
        template.find('#gradoC').value=gradoTmp;
    }
});