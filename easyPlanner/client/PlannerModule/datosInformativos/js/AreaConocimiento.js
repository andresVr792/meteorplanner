Template.AreaConocimiento.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('areasConocimiento');
        opEditarPlan=Session.get('op-editarPlan');
        /*if(!opEditarPlan=='open') {
         Meteor.call('updateAreaConocimiento', Session.get('currentPlan')._id, Session.get('logedUser').profile.areaConocimiento);
         var area = Areas.findOne({value: Session.get('logedUser').profile.areaConocimiento});
         Session.setPersistent('areaObjTmp', area);
         }*/
    });
});
Template.AreaConocimiento.helpers({
        areas: ()=>{
        return Areas.find({});
},
areaPlan:function(){

    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var area = PlanAnual.findOne({_id: currentPlanAC._id});
        Session.setPersistent('areaTmp', area.areaConocimiento);
        var areaObj = Areas.findOne({value: area.areaConocimiento});
        Session.setPersistent('areaObjTmp', areaObj);
        return PlanAnual.findOne({_id: Session.get('currentPlan')._id});


    }
},
logedUserArea:function () {
    return "SELECCCIONAR ÁREA"

},
modoEdicionAC:()=>{
    currentPlanAC=Session.get('currentPlan');
    if(!currentPlanAC=='') {
        var area = PlanAnual.findOne({_id: currentPlanAC._id});
        return !area.areaConocimiento;

    }
}
});
Template.AreaConocimiento.events({
    'change form#area #areaConocimiento': function(event,template){
        var areaTmp=template.find('#areaConocimiento').value;
        Session.setPersistent('areaTmp',areaTmp);
        var area=Areas.findOne({value:areaTmp});
        Session.setPersistent('areaObjTmp',area);
        Meteor.call('updateAreaConocimiento',Session.get('currentPlan')._id,areaTmp);
        $('#areaConocimiento').val($('#areaConocimiento > option:first').val());
        Meteor.call('updateAsignatura',Session.get('currentPlan')._id,'');
        template.find('#areaC').value=areaTmp;
    }
});
