
Template.DatosInformativos.helpers({
    showHideDatosInformativos:function () {
        return Session.get('shDatosInformativos')==='open';
    }
});
Template.DatosInformativos.events({
    'click .showDatosInformativos':function () {
        Session.set('shDatosInformativos','open');
    },
    'click .hideDatosInformativos':function () {
        Session.set('shDatosInformativos','');
    }
});

