Template.ControlNavElemental.rendered=function () {
    if(!this._rendered){
        this._rendered=true;
        // Session.set('nav-nuevoPlanAnual','open');
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','');
       // this.subscribe('allplanAnual');
    }
}

Template.ControlNavElemental.onCreated(function () {
    this.autorun(()=>{

        this.subscribe('planAnual',Meteor.userId());
}) ;
});

Template.ControlNavElemental.helpers({
    unidad:function () {
        return Session.get('unidad')=='';
    }
});

Template.ControlNavElemental.events({
    'click .op-AgregarPlanE':function (){
        Session.set('op-agregarPlan','open');
        Session.set('op-editarPlan','');
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('aaa',null);
        Session.set('gradoTmp',null);
        Session.set('dimensionITmp',null);
        Session.set('areaTmp',null);
        Session.set('currentPlan',null);
        Session.set('areaObjTmp',null);
        Session.set('subnivelSingle',null);
        Session.set('subnivelTmp',null);
        Session.set('asignaturaTmp',null);
        Session.set('nav-editarPlan',null);
        Session.set('op-nuevoPlan',null);
        Session.set('currentUnity',null);
        Session.set('currentPlanId',null);
        Session.set('currentPlanNombre',null);
        Session.set('asignaturaTmpText',null);
        Session.set('tipoEvaluacion',null);
        $('#configuracion').hide();
        Session.set('compartir',"null");
        Session.set('sec-contenidosConceptules',null);
    },
    'click .op-EditarPlanE':function(){
        Session.set('op-agregarPlan','');
        Session.set('op-editarPlan','open');
        Session.set('nav-nuevoPlanAnual','open');
        Session.set('aaa',null);
        Session.set('gradoTmp',null);
        Session.set('dimensionITmp',null);
        Session.set('areaTmp',null);
        Session.set('currentPlan',null);
        Session.set('areaObjTmp',null);
        Session.set('subnivelSingle',null);
        Session.set('subnivelTmp',null);
        Session.set('asignaturaTmp',null);
        Session.set('nav-editarPlan',null);
        Session.set('op-nuevoPlan',null);
        Session.set('currentUnity',null);
        Session.set('currentPlanId',null);
        Session.set('currentPlanNombre',null);
        Session.set('asignaturaTmpText',null);
        Session.set('tipoEvaluacion',null);
        Session.set('sec-contenidosConceptules',null);
        $('#configuracion').hide();
        Session.set('nav-configuracion',null);

        Session.set('compartir',null);

    },
    'click .op-CompartidoPlanE':function () {
        if(Session.get('plan')=="elemental"){
            Session.set('op-agregarPlan','');
            Session.set('op-editarPlan','open');
            Session.set('nav-nuevoPlanAnual','open');
            Session.set('aaa',null);
            Session.set('gradoTmp',null);
            Session.set('dimensionITmp',null);
            Session.set('areaTmp',null);
            Session.set('currentPlan',null);
            Session.set('areaObjTmp',null);
            Session.set('subnivelSingle',null);
            Session.set('subnivelTmp',null);
            Session.set('asignaturaTmp',null);
            Session.set('nav-editarPlan',null);
            Session.set('op-nuevoPlan',null);
            Session.set('currentUnity',null);
            Session.set('currentPlanId',null);
            Session.set('currentPlanNombre',null);
            Session.set('asignaturaTmpText',null);
            Session.set('tipoEvaluacion',null);

            $('#configuracion').hide();
            Session.set('nav-configuracion',null);
            Session.set('compartir',"compartidoElemental");
            var coleccion=PlanAnual.find({
                compartido: [
                    {
                        idDocente: Meteor.userId(),
                        correoElectronico: Session.get('logedUser').emails[0].address
                    }
                ]
                , tipo: "elemental"
            }).count();

        }else
        if(Session.get('plan')=="anual"){
            Session.set('op-agregarPlan','');
            Session.set('op-editarPlan','open');
            Session.set('nav-nuevoPlanAnual','open');
            Session.set('aaa',null);
            Session.set('gradoTmp',null);
            Session.set('dimensionITmp',null);
            Session.set('areaTmp',null);
            Session.set('currentPlan',null);
            Session.set('areaObjTmp',null);
            Session.set('subnivelSingle',null);
            Session.set('subnivelTmp',null);
            Session.set('asignaturaTmp',null);
            Session.set('nav-editarPlan',null);
            Session.set('op-nuevoPlan',null);
            Session.set('currentUnity',null);
            Session.set('currentPlanId',null);
            Session.set('currentPlanNombre',null);
            Session.set('asignaturaTmpText',null);
            $('#configuracion').hide();
            Session.set('nav-configuracion',null);
            Session.set('compartir',"compartidoAnual");
        }
        var coleccion=PlanAnual.find({
            compartido: [
                {
                    idDocente: Meteor.userId(),
                    correoElectronico: Session.get('logedUser').emails[0].address
                }
            ], tipo: "anual"

        }).count();

    }

});