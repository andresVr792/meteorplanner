Template.PrintReportNav.onCreated(function () {
    this.autorun(()=>{


    }) ;
});

Template.PrintReportNav.events({
    'click .regresar':function () {


            sAlert.info('<div class="text-center"><i class="fa fa-window-close"></i> Regresando!</div> <br> <div class="text-center">Recuerde que toda la información ha sido guardada exitosamente!<br> </div>', {
                effect: 'bouncyflip', position: 'top-right', timeout: 3000, onClose: function () {
                    FlowRouter.go('planAnual');
                }, stack: false, html: true, offset: '50px'
            });

            Session.set('op-agregarPlan', '');
            Session.set('op-editarPlan', '');
            Session.set('nav-nuevoPlanAnual', '');
            Session.set('aaa', null);
            Session.set('gradoTmp', null);
            Session.set('dimensionITmp', null);
            Session.set('areaTmp', null);
            Session.set('currentPlan', null);

            Session.set('currentUnity', null);
            Session.set('currentPlanId', null);
            Session.set('currentPlanNombre', null);
            Session.set('asignaturaTmpText', null);


            Session.set('areaObjTmp', null);
            Session.set('subnivelSingle', null);
            Session.set('subnivelTmp', null);
            Session.set('asignaturaTmp', null);
            Session.set('nav-editarPlan', null);
            Session.set('op-nuevoPlan', null);


    },
    'click .generarPDF':function(){

        var docDefinition={content:'esto es un ejemplo con pdfmake'}
        pdfMake.createPdf(docDefinition).open();
        pdfMake.createPdf(docDefinition).print();
        pdfMake.createPdf(docDefinition).dowload('sample.pdf');
        /*if (Session.get('unidad') == 'active'&&Session.get('plan')=='anual') {
            NProgress.start(); //this starts the progress bar


            Meteor.call('phantom',
                'http://localhost:3000/educador/reporteMicroParametros/' +Session.get('currentPlanId') +'/' + Session.get('currentUnityId'),
                Session.get('currentPlanId') + 'planUnidadNumeroUnidad'+Session.get('currentUnity').numeroUnidad, Meteor.userId(), Session.get('currentPlanId'),
                function (error, result) {
                    setTimeout(function () {
                        sAlert.info('<div class="text-center"><i class="fa fa-info-circle"></i> Información!</div> <br> <div class="text-center">Generando Reporte!<br>  <strong>Espere por favor!</strong></div>', {
                            effect: 'slide', position: 'top-right', timeout: 5000, onClose: function () {
                                NProgress.done();
                                FlowRouter.go('reportePrueba');
                            }, stack: false, html: true, offset: '50px'
                        });
                    }, 5000);
                });


            Session.set('op-agregarPlan', '');
            Session.set('op-editarPlan', '');
            Session.set('nav-nuevoPlanAnual', '');
            Session.set('aaa', null);
            Session.set('gradoTmp', null);
            Session.set('dimensionITmp', null);
            Session.set('areaTmp', null);
            Session.set('currentPlan', null);

            Session.set('currentUnity', null);
            Session.set('currentPlanId', null);
            Session.set('currentPlanNombre', null);
            Session.set('asignaturaTmpText', null);


            Session.set('areaObjTmp', null);
            Session.set('subnivelSingle', null);
            Session.set('subnivelTmp', null);
            Session.set('asignaturaTmp', null);
            Session.set('nav-editarPlan', null);
            Session.set('op-nuevoPlan', null);

        }
        if(Session.get('unidad') == ''&&Session.get('plan')=='anual') {
            NProgress.start(); //this starts the progress bar

            Meteor.call('phantom',
                'http://localhost:3000/educador/reporteAnualParametros/' + Session.get('currentPlanId') +'/' + Session.get('currentUnityId'),
                Session.get('currentPlanId') + 'anual', Meteor.userId(), Session.get('currentPlanId'),
                function (error, result) {
                    setTimeout(function () {
                        sAlert.info('<div class="text-center"><i class="fa fa-info-circle"></i> Información!</div> <br> <div class="text-center">Genetando Reporte!<br>  <strong>Espere por favor!</strong></div>', {
                            effect: 'slide', position: 'top-right', timeout: 5000, onClose: function () {
                                NProgress.done();
                                FlowRouter.go('reportePrueba');
                            }, stack: false, html: true, offset: '50px'
                        });
                    }, 5000);
                });

            Session.set('op-agregarPlan', '');
            Session.set('op-editarPlan', '');
            Session.set('nav-nuevoPlanAnual', '');
            Session.set('aaa', null);
            Session.set('gradoTmp', null);
            Session.set('dimensionITmp', null);
            Session.set('areaTmp', null);
            Session.set('currentPlan', null);

            Session.set('currentUnity', null);
            Session.set('currentPlanId', null);
            Session.set('currentPlanNombre', null);
            Session.set('asignaturaTmpText', null);


            Session.set('areaObjTmp', null);
            Session.set('subnivelSingle', null);
            Session.set('subnivelTmp', null);
            Session.set('asignaturaTmp', null);
            Session.set('nav-editarPlan', null);
            Session.set('op-nuevoPlan', null);
        }
        if(Session.get('plan')=='elemental'){
            NProgress.start(); //this starts the progress bar

            Meteor.call('phantom',
                'http://localhost:3000/educador/reportePlanPrimero/' +Session.get('currentPlanId') +'/' + Session.get('currentUnityId'),
                Session.get('currentPlanId') + 'planUnidadElementalNumeroUnidad'+Session.get('currentUnity').numeroUnidad, Meteor.userId(), Session.get('currentPlanId'),
                function (error, result) {
                    setTimeout(function () {
                        sAlert.info('<div class="text-center"><i class="fa fa-info-circle"></i> Información!</div> <br> <div class="text-center">Genetando Reporte!<br>  <strong>Espere por favor!</strong></div>', {
                            effect: 'slide', position: 'top-right', timeout: 5000, onClose: function () {
                                NProgress.done();
                                FlowRouter.go('reportePrueba');
                            }, stack: false, html: true, offset: '50px'
                        });
                    }, 5000);
                });

            Session.set('op-agregarPlan', '');
            Session.set('op-editarPlan', '');
            Session.set('nav-nuevoPlanAnual', '');
            Session.set('aaa', null);
            Session.set('gradoTmp', null);
            Session.set('dimensionITmp', null);
            Session.set('areaTmp', null);
            Session.set('currentPlan', null);

            Session.set('currentUnity', null);
            Session.set('currentPlanId', null);
            Session.set('currentPlanNombre', null);
            Session.set('asignaturaTmpText', null);


            Session.set('areaObjTmp', null);
            Session.set('subnivelSingle', null);
            Session.set('subnivelTmp', null);
            Session.set('asignaturaTmp', null);
            Session.set('nav-editarPlan', null);
            Session.set('op-nuevoPlan', null);

        }*/
         }



});
