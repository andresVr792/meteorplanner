Template.ObjetivoIntegradores.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.ObjetivoIntegradores.events({
        'click .btnAgregarObjetivo':()=> {
        Session.set('nav-objetivoIntegrador', 'open');
},
'click .eliminarObjetivo':function () {
    Meteor.call('updatePullObjetivosIntegradores',Session.get('currentPlan')._id,this.texto);
    porcentajePlanCompleto=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado;
    objetivosIntegradores=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).objetivosIntegradores;
    if(countCursor(objetivosIntegradores)<=1){
        if(PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado>10)
            Meteor.call('updatePlanCompletado',Session.get('currentPlan')._id,porcentajePlanCompleto-5);
    }

}

});
Template.ObjetivoIntegradores.helpers({
    objetivosIntegradores:function(){
        currentPlanIO=Session.get('currentPlan');
        if(!currentPlanIO=='') {
            var objIntegradores = PlanAnual.findOne({_id: currentPlanIO._id});
            return objIntegradores;
        }
    }
});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}