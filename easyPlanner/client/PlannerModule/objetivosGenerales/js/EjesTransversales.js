Template.EjesTransversales.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.EjesTransversales.events({
        'click .btnAgregarEje':function(event,template) {
            var eje=template.find('#inputEjeTransversal').value;
            if(eje!="") {
                Meteor.call('updatePushEjesTransersales', Session.get('currentPlan')._id, eje);
                $('#inputEjeTransversal').val("");
            }
            else{
                Bert.alert('Advertencia: No se acepta el ingreso de campos vacios','danger');

            }
         },
        'click .eliminarEjeTransversal':function () {
            Meteor.call('updatePullEjesTransersales',Session.get('currentPlan')._id,this.texto);

        }

});
Template.EjesTransversales.helpers({
    ejesTransversales:function(){

        var listEjesTransversales=   PlanAnual.findOne({_id:Session.get('currentPlan')._id}).ejesTransversales;
        return listEjesTransversales;
    }
});