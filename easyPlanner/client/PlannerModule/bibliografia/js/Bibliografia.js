Template.Bibliografia.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.Bibliografia.events({
        'click .btnBibliografia':function(event,template) {
            var bibliografia=template.find('#inputBibliografia').value;
            referenciasbibliograficas=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).bibliografia;
            porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
            if (countCursor(referenciasbibliograficas)==0) {
                Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 20);
                Meteor.call('updatePushBibliografia', Session.get('currentPlan')._id, bibliografia);
                $('#inputBibliografia').val("");

            }
            else {
                if (bibliografia != "") {
                    Meteor.call('updatePushBibliografia', Session.get('currentPlan')._id, bibliografia);
                    $('#inputBibliografia').val("");
                }
                else {
                    sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center"> No se permiten campos vacíos</div>', {
                        effect: 'bouncyflip',
                        position: 'top-right',
                        timeout: 3000,
                        onRouteClose: false,
                        stack: false,
                        html: true,
                        offset: '50px'
                    });


                }
            }
         },
        'click .eliminarBibliografia':function () {
            Meteor.call('updatePullBibliografia',Session.get('currentPlan')._id,this.texto);
            porcentajePlanCompleto=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado;
            referenciasbibliograficas=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).bibliografia;
            if(countCursor(referenciasbibliograficas)==1){
                if(PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado>10)
                    Meteor.call('updatePlanCompletado',Session.get('currentPlan')._id,porcentajePlanCompleto-20);
            }
        }

});
Template.Bibliografia.helpers({
    bilbiografiaList:function(){

        currentPlanB=Session.get('currentPlan');
        if(!currentPlanB=='') {
            var bibliografia = PlanAnual.findOne({_id: currentPlanB._id}).bibliografia;
            return bibliografia;
        }
    }
});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}