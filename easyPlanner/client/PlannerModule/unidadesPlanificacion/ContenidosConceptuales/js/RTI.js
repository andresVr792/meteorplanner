Template.RTI.onCreated(function(){
    var self=this;
    self.autorun(function () {


    });
});
Template.RTI.events({
    'click .btnRTI':function(event,template) {
        Session.set('nav-rti','open');
        return false;
    },
    'click .eliminarRTI':function () {
        Meteor.call('actualizarRTIPull',Session.get('currentContenidoConceptual'),this.texto,this.instrumento);
        return false;
    }


});
Template.RTI.helpers({
    rti:function(){
        currentContenidoConceptualRTI=Session.get('currentContenidoConceptual');
        if(!currentContenidoConceptualRTI=='') {
            var listActividades = ContenidosConceptualesUnidad.findOne({_id: currentContenidoConceptualRTI._id}).rtis;
            return listActividades;
        }
    }
});