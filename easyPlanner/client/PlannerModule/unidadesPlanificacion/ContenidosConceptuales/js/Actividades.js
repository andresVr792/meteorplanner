Template.Actividades.onCreated(function(){
    var self=this;
    self.autorun(function () {


    });
});
Template.Actividades.events({
    'click .btnActividades':function(event,template) {
        Session.set('nav-actividades','open');
        return false;
    },
    'click .eliminarActividad':function () {
        Meteor.call('actualizarActividadPull',Session.get('currentContenidoConceptual'),this.texto,this.detalle);
        return false;

    },
    'click .editarActividad':function () {
       Session.set('nav-actividades-edit','open');

       Session.set('actividadEdit',this.texto);
        Session.set('actividadDetalleEdit',this.detalle);

        return false;

    }

});
Template.Actividades.helpers({
    actividadesList:function(){
        currentContentidoConceptual=Session.get('currentContenidoConceptual');
        if(!currentContentidoConceptual=='') {
            var listActividades = ContenidosConceptualesUnidad.findOne({_id: currentContentidoConceptual._id});
            return listActividades;
        }
    }
});