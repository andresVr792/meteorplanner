Template.ContenidosUnidad.events({
    'click .btn-terminarDespuesCC':function () {
        Session.set('sec-contenidosConceptules','');
        sAlert.info('<div class="text-center"><i class="fa fa-info-circle"></i> Información!</div> <br> <div class="text-center">Recuerde completar ésta sección antes de imprimir el plan</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
        Session.set('currentContenidoConceptual',null);

        return false;
    },
    'click .btn-guardarUnidadCC':function () {
        Session.set('sec-contenidosConceptules','');
        sAlert.success('<div class="text-center"><i class="fa fa-info-circle"></i> Conformación!</div> <br> <div class="text-center">La información fue almacenada exitosamente</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
        Session.set('currentContenidoConceptual',null);

        return false;
    }
});