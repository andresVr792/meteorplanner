Template.UnidadesPlan.events({
    'click .editarPlanUnidad':function () {
        Session.set('nav-planificarUnidadAnual','open');
        Session.setPersistent('currentUnity',this);
        Session.setPersistent('currentUnityId',this._id);
    }
});
Template.UnidadesPlan.onRendered(function(){
    var self=this;
    self.autorun(function () {
        currentPlanUnidadesPlan=Session.get('currentPlan');
        if(!currentPlanUnidadesPlan=='') {
            self.subscribe('unidad', Session.get('currentPlan')._id);

        }

    });
});
Template.UnidadesPlan.helpers({
        unidadesP:function(){
        return Unidad.find({},{sort:{numeroUnidad:1}});
},
editMode:function(){
    return Session.get('nav-planificarUnidadAnual')=='open';
},
modoCompartidoUnidad:function () {
    return Session.get('compartir') !='compartidoAnual';
}


});
