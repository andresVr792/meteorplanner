Template.ContenidoConceptual.helpers({
        contenidosConceptualesUnidad:function (){
        currentUnityCC=Session.get('currentUnity');
        if(!currentUnityCC=='') {
            return ContenidosConceptualesUnidad.find({idUnidad:currentUnityCC._id});
        }
    },
    detalleContenido:function () {
        return Session.get('sec-contenidosConceptules')=='active';
    },
    editarContenido:function () {
        return Session.get('editarContenido')=='active';
    },


});
Template.ContenidoConceptual.events({
    'click .btnAgregarContenido':function () {
        Session.set('nav-contenidos', 'open');
        Session.set('editarContenido','');
        return false;
    },
    'click .eliminarContenido':function () {
        Meteor.call('eliminarContenido',this._id);
        Meteor.call('actualizarContenidoPull',Session.get('currentUnity'),this.texto);
        Session.set('sec-contenidosConceptules','');
        Session.set('currentContenidoConceptual',null);


    },
    'click .editarContenido':function () {
        Session.set('nav-contenidos', 'edit');
        Session.set('contenidoConceptualtmp',this.texto);
        Session.set('contenidoConceptualDetalletmp',this.detalle);
        Session.set('sec-contenidosConceptules','active');
        Session.set('currentContenidoConceptual',this);
        Session.set('editarContenido','active');
    },
    'click .numeroContenido':function () {
        Meteor.call('actualizarDuracionContenidoConceptualUnidad', Session.get('currentContenidoConceptual')._id, 0);

    }
    ,
    'input #productoContenido':function (event,template) {
        var detalle=template.find('#productoContenido').value;

        Meteor.call('actualizarProducto',Session.get('currentContenidoConceptual')._id,detalle);


    },
    'input #tiempoContenido':function (event,template) {
        var tiempo=template.find('#tiempoContenido').value;

        var tiempoUnidad=Session.get('currentUnity').duracionUnidad;
        var tiempototalIngresado=tiempoTotal(ContenidosConceptualesUnidad.find({idUnidad:Session.get('currentUnityId')}));
        if((parseInt(tiempototalIngresado)+parseInt(tiempo))<=parseInt(tiempoUnidad)) {

            Meteor.call('actualizarDuracionContenidoConceptualUnidad', Session.get('currentContenidoConceptual')._id, tiempo);
            Session.set('editarContenido','');

        }
        else{
            sAlert.error('<div class="text-center"><i class="fa fa-window-close"></i> Error!</div> <br> <div class="text-center">Tiempo limite de unidad excedido! <br> No se permite que la suma de los tiempos de los contenidos conceptuales sea mayor que el de la unidad</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 5000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });
            template.find('#tiempoContenido').value="";
        }
    }
});

Template.ContenidoConceptual.onCreated(function(){
    var self=this;
    self.autorun(function () {
        currentPlanContenidos=Session.get('currentPlan');
        currentUnity=Session.get('currentUnity');
        if(!currentUnity=='')
//            self.subscribe('contenidosConceptualesUnidadFilter',currentPlanContenidos);

        self.subscribe('contenidosConceptualesUnidadFilterUnidad',currentUnity);
        self.subscribe('areasConocimiento');
        self.subscribe('todasAsignaturas');
        self.subscribe('subnivel');
        currentPCC=Session.get('currentPlan');
        if(!currentPCC=='') {
            var plan = PlanAnual.findOne({_id: currentPCC._id});

            var asignatura = Asignaturas.findOne({value: plan.asignatura});
            Session.set("asignaturaTmp", asignatura);
            console.log(currentPCC.asignatura);
            Session.setPersistent('subnivelSingle', plan.subnivelEducativo);
            Session.setPersistent('subnivelTmp', Subnivel.findOne({value: plan.subnivelEducativo}));
        }
    });
});
function tiempoTotal(collection){
    var suma=0;
    collection.forEach(function(doc){
        suma=suma+doc.duracionContenido;
    });
    return suma;
}
