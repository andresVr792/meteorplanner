Template.Unidades.events({
    'click .btn-AgregarUnidad':function () {
        Session.set('nav-nuevaUnidad','open');

        return false;
    },
    'click .eliminarPlan':function(){
        Meteor.call('eliminarUnidad',this._id);
        porcentajePlanCompleto=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado;
        unidades=Unidad.find({idPlanAnual:Session.get('currentPlan')._id});
        if(countCursor(unidades)==0){
           if(PlanAnual.findOne({_id:Session.get('currentPlan')._id}).planCompletado>30)
                Meteor.call('updatePlanCompletado',Session.get('currentPlan')._id,porcentajePlanCompleto-50);
        }
    },
    'click .editarPlan':function () {
        Session.set('nav-editarPlan','open');
        Session.setPersistent('currentUnity',this);
        Session.set('nav-nuevaUnidad', '');
    }
});
Template.Unidades.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('unidad',Session.get('currentPlan')._id);



    });
});
Template.Unidades.helpers({
    unidadesPlan:()=>{
        return Unidad.find({},{sort:{numeroUnidad:1}});
    },
nuevaUnidadController:function(){
        return   Session.get('nav-nuevaUnidad')=='active';
},
editMode:function(){
    return Session.get('nav-editarPlan')=='open';
}

});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}