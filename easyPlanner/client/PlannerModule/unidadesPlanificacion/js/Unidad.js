Template.Unidad.helpers({
        editMode:()=>{
                return Session.get('nav-editarPlan')=='open';
        },
        currentUnityG:()=>{
                currentUnityUG=Session.get('currentUnity');
                if(!currentUnityUG=='') {
                    var unidadPlanEditar = Unidad.findOne({_id: currentUnityUG._id});
                    return unidadPlanEditar.numeroUnidad + ' "' + unidadPlanEditar.titulo + '"';
                }
        },
        isElemental:()=>{
                return Session.get('plan')=='elemental';
},
isAnual:()=>{
        return Session.get('plan')=='anual';
}
});
Template.Unidad.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('unidad',Session.get('currentPlan')._id);



    });
});
Template.Unidad.events({
        'click .btn-guardarUnidad':function () {
                sAlert.info('<i class="fa fa-info-circle"></i> <br>Los Datos de la Unidad Han sido Guardados con exito!', {effect: 'bouncyflip', position: 'top-right', timeout: 4000, onRouteClose: false, stack: false,html:true, offset: '50px'});
                Session.set('nav-editarPlan','');
            Session.set('nav-nuevaUnidad','');

            unidades=Unidad.find({idPlanAnual:Session.get('currentPlan')._id});
            porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
            if (countCursor(unidades)<=2) {
                    if(porcentajePlanCompleto<80)
                Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 25);

            }
                return false;

        },
        'click .btn-terminarDespues':function () {
            sAlert.info('<i class="fa fa-info-circle"></i>Recuerde completar ésta sección para terminar le plan!', {effect: 'bouncyflip', position: 'top-right', timeout: 4000, onRouteClose: false, stack: false,html:true, offset: '50px'});
            Session.set('nav-editarPlan','');
            Session.set('nav-nuevaUnidad','');
            return false;
        }
});
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}
