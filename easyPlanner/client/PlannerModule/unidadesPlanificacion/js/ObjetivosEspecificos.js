
Template.ObjetivoEspecificos.helpers({
    objetivosEspecificos:()=>{
        currentUnityOE=Session.get('currentUnity');
        if(!currentUnityOE=='') {
            return Unidad.findOne({_id: Session.get('currentUnity')._id}).objetivosEspecificos;
        }
    }
});
Template.ObjetivoEspecificos.events({
    'click .btnAgregarObjetivoEspecifico':function () {
        Session.set('nav-objetivoEspecificos', 'open');

        return false;
    },
    'click .eliminarObjetivoEspecifico':function () {
        Meteor.call('actualizarObjetivoEspecificoPull',Session.get('currentUnity')._id,this.texto);

    }
});

