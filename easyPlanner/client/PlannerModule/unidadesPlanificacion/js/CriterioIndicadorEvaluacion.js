
Template.CriteriosIndicadoresEvaluacion.helpers({
        indicadores:()=>{
            currentUnityCIE=Session.get('currentUnity');
            if(!currentUnityCIE=='') {
                return Unidad.findOne({_id: currentUnityCIE._id}).criteriosindicadoresEvaluacion;
            }
}
});
Template.CriteriosIndicadoresEvaluacion.events({
    'click .btnAgregarIndicador':function () {
        Session.set('nav-indicadores', 'open');



        return false;
    },
    'click .eliminarIndicador':function () {
        Meteor.call('actualizarcriteriosindicadoresEvaluacionPull',Session.get('currentUnity')._id,this.texto);

    }

});

