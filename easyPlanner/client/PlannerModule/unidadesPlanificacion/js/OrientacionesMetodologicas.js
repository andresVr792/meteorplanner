Template.OrientacionesMetodologicas.onCreated(function(){
    var self=this;
    self.autorun(function () {

        //self.subscribe('objetivosIntegradores');


    });
});
Template.OrientacionesMetodologicas.events({
        'click .btnAgregarOrientacion':function(event,template) {
            var orientacion=template.find('#inputOrientacionMetologica').value;

            if(orientacion!="") {
                Meteor.call('actualizarOrientacionMetodologicaPush', Session.get('currentUnity')._id, orientacion);
               $('#inputOrientacionMetologica').val("");
              }
              else{
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});



            }
            return false;
         },
        'click .eliminarOrientacion':function () {
            Meteor.call('actualizarOrientacionMetodologicaPull',Session.get('currentUnity')._id,this.texto);

        }

});
Template.OrientacionesMetodologicas.helpers({
    orientaciones:function(){
        currentUnityOM=Session.get('currentUnity');
        if(!currentUnityOM=='') {
            var listOrientacionMetodologica = Unidad.findOne({_id: currentUnityOM._id}).orientacionesMetodologicas;
            return listOrientacionMetodologica;
        }
    }
});