Template.ConfiguracionCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('allUsers');
    });
});


Template.ConfiguracionCatalog.helpers({
    docentes: function () {
        return Meteor.users.find({roles:{$in:['educador']}});

    },
    listaCompartido:function(){
        currentPlan=Session.get('currentPlan');
        if(!currentPlan=="") {
            return PlanAnual.findOne({_id: currentPlan._id});
        }
    },
    nombre:function(){
        var compartido=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).compartido;
        return Meteor.users.finOne({_id:compartido.idDocente});
    }
});
Template.ConfiguracionCatalog.events({
        'click .close-configuracion ': () => {
        Session.set('nav-configuracion', '');

},
'click .btn-agregarCompartido': function (event, template) {
     profesorMail=template.find('#listaDocentes').value;
    compartidos=PlanAnual.findOne({_id:Session.get('currentPlan')._id}).compartido;
     usuario=Meteor.users.findOne({emails:
     [
         {
             address:profesorMail,
             verified:false
         }
     ]
     });
    if(!findRepetitions(compartidos,usuario._id)){
        Meteor.call('updatePushCompartido', Session.get('currentPlan')._id, usuario._id, profesorMail);
        sAlert.success('<div class="text-center"><i class="fa fa-info-circle"></i> Información!</div> <br> <div class="text-center">El plan se ha conpartido satisfactoriamente!</div>', {
            effect: 'slide', position: 'top-right', timeout: 5000, onClose: false, stack: false, html: true, offset: '50px'
        });
        template.find('#listaDocentes').value=null;

    }else{
        sAlert.error('<div class="text-center"><i class="fa fa-close"></i> Error!</div> <br> <div class="text-center">El plan ya se encuentra compartido con ésta persona!</div>', {
            effect: 'slide', position: 'top-right', timeout: 5000, onClose: false, stack: false, html: true, offset: '50px'
        });
        template.find('#listaDocentes').value=null;

    }
     return false;
    },
'click .eliminarCompartido':function(){
    Meteor.call('updatePullCompartido',Session.get('currentPlan')._id,this.idDocente,this.correoElectronico);

}

});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.idDocente==value;
    });

}