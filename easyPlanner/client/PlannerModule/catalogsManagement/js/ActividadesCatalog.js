Template.ActividadesCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('allActividades');
        if( Session.get('nav-actividades-edit')=='open'){
            Session.set('nav-actividades','open');

            $('#actividades').val( Session.get('actividadEdit'));
            $('#txtAreaDetalleActividad').val( Session.get('actividadDetalleEdit'));
        }else{
            $('#actividades').val('');
            $('#txtAreaDetalleActividad').val( '');
        }

    });
});
Template.ActividadesCatalog.rendered = function () {
    $("#slcActividades").select2({
        placeholder: "Selecciona/busca la actividad",
        language: "es",
        tags:true,
        allowClear:true


    });
}

Template.ActividadesCatalog.helpers({
    actividades: function () {
        return Actividades.find();
    }

});
Template.ActividadesCatalog.events({
        'click .close-actividad ': () => {
        Session.set('nav-actividades', '');
        Session.set('nav-actividades-edit','');
       // $("#slcActividades").empty().trigger('change');



},
'click .btn-AgregarActividad': function (event, template) {

    if( Session.get('nav-actividades-edit')=='open') {
        var actividadTmp = template.find('#slcActividades').value;
        var detalleTmp = template.find('#txtAreaDetalleActividad').value;

        Meteor.call('actualizarActividadPull',Session.get('currentContenidoConceptual'),Session.get('actividadEdit'),Session.get('actividadDetalleEdit'));
        Meteor.call('actualizarActividadPush', Session.get('currentContenidoConceptual'), actividadTmp, detalleTmp);

    }
    else{
        var actividadTmp = template.find('#slcActividades').value;

        var detalleTmp = template.find('#txtAreaDetalleActividad').value;
        var actividades = ContenidosConceptualesUnidad.findOne({_id: Session.get('currentContenidoConceptual')._id}).actividades;
        if (!findRepetitions(actividades, actividadTmp) && detalleTmp != "" && actividadTmp!='') {

            Meteor.call('actualizarActividadPush', Session.get('currentContenidoConceptual'), actividadTmp, detalleTmp);

        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">La destreza ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }
    }
        template.find('#txtAreaDetalleActividad').value = "";
  //  $("#slcActividades").empty().trigger('change');
    Session.set('nav-actividades', '');
        return false;



},
'click .actividades':function(events,template){
    $("#slcActividades").click().trigger('change');


},
'click .txtAreaDetalleActividad':function(events,template){
    template.find('#txtAreaDetalleActividad').value = "";
}

});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}