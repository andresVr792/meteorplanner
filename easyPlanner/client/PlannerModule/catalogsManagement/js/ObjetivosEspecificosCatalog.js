Template.ObjetivosEspecificosCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('objetivosEspecificosFiltro',Session.get('areaObjTmp'),Session.get('asignaturaTmp'),Session.get('subnivelTmp'));

    });
});

Template.ObjetivosEspecificosCatalog.rendered = function () {
    $("#slcObjetivoEspecifico").select2({
        placeholder: "Selecciona/busca objetivo especifico",
        language: "es"

    });
}
Template.ObjetivosEspecificosCatalog.helpers({
    objetivos: function () {
        return ObjetivosEspecificos.find();
    }
});
Template.ObjetivosEspecificosCatalog.events({
        'click .close-ingresarObjetivoEspecifico ': function (event,template){
        Session.set('nav-objetivoEspecificos', '');
        template.find('#txtAreaObjetivoEspecifico').textContent='';

},
'click .btn-AgregarObjetivoEspecifico': function (event, template) {

    //var objTmp = $('input[name=objetivoGeneral]').val();
    var objTmp = template.find('#slcObjetivoEspecifico').value;
    var objEspecifico=   Unidad.findOne({_id:Session.get('currentUnity')._id}).objetivosEspecificos;
    if(!findRepetitions(objEspecifico,objTmp)&&objTmp!="") {
        Meteor.call('actualizarObjetivoEspecificoPush',Session.get('currentUnity')._id,objTmp);

    }
    else{
        sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El objetivo ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

      }
    Session.set('nav-objetivoEspecificos', '');
    template.find('#txtAreaObjetivoEspecifico').textContent='';
    return false;



},
'click .btn-verObjetivoEspecifico':function(event, template){
    Session.set('nav-detalle','open');
    var objTmp = $('#slcObjetivoEspecifico').val();
    template.find('#txtAreaObjetivoEspecifico').textContent=objTmp;
    return false;
}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}