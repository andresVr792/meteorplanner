Template.NuevoPlan.onCreated(function () {
    this.autorun(()=>{

    this.subscribe('planAnual',Meteor.userId());
}) ;
});

Template.NuevoPlan.helpers({
    ocultarNuevoPlanFrm:function () {
        return Session.get('nav-nuevoPlanAnual')==='';
    },
    ocultarEditarPlan:function () {
        return Session.get('nav-EditarPlan')==='';

    }
});
Template.NuevoPlan.events({

'click .btn-NuevoPlan':function (event,template) {

    if(Session.get('plan')=="anual") {
        var planesCount=PlanAnual.find({nombre:template.find('#inputNumbrePlan').value}).count();
        if(planesCount==0&&!template.find('#inputNumbrePlan').value=='') {
            PlanAnual.insert({
                nombre: template.find('#inputNumbrePlan').value,
                autor: [Session.get('logedUser')],
                tipo: "anual",
                planCompletado: 10,
                docentes: [],
                paralelos: [],
                objetivosAreaConocimiento: [],
                objetivosIntegradores: [],
                bibliografia: [],
                adaptacionesCurriculares: [],
                compartido: []

            });
            Session.set('nav-nuevoPlanAnual','');
            Session.setPersistent('op-agregarPlan','');
            Session.setPersistent('op-nuevoPlan','active');

        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El nombre del plan ya existe, ingrese uno nuevo! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
            template.find('#inputNumbrePlan').value='';
        }
    }
    if(Session.get('plan')=="elemental") {
        var planesCount=PlanAnual.find({nombre:template.find('#inputNumbrePlan').value}).count();
        if(planesCount==0&&!template.find('#inputNumbrePlan').value=='') {
            PlanAnual.insert({
                nombre: template.find('#inputNumbrePlan').value,
                autor: [Session.get('logedUser')],
                tipo: "elemental",
                planCompletado: 40,
                docentes: [],
                paralelos: [],
                adaptacionesCurriculares: [],
                compartido: [],
                curriculoIntegrador:[]

            });
            Session.set('nav-nuevoPlanAnual','');
            Session.setPersistent('op-agregarPlan','');
            Session.setPersistent('op-nuevoPlan','active');
        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El nombre del plan ya existe, ingrese uno nuevo! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
            template.find('#inputNumbrePlan').value='';
        }
    }
    if(Session.get('plan')=="proyecto") {
        var planesCount=PlanAnual.find({nombre:"pro_"+template.find('#inputNumbrePlan').value,tipo:"proyecto"}).count();
        if(planesCount==0&&!template.find('#inputNumbrePlan').value=='') {
            PlanAnual.insert({
                nombre: "pro_"+template.find('#inputNumbrePlan').value,
                autor: [Session.get('logedUser')],
                tipo: "proyecto",
                docentes: [],
                planCompletado: 40,
                paralelos: [],
                hiloConductor: [],
                compartido: [],
                metaComprension:[]

            });
            Session.set('nav-nuevoPlanAnual','');
            Session.setPersistent('op-agregarPlan','');
            Session.setPersistent('op-nuevoPlan','active');

        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El nombre del plan ya existe, ingrese uno nuevo! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
            template.find('#inputNumbrePlan').value='';
        }
    }
    if(Session.get('plan')=="trabajoCooperativo") {
        var planesCount=PlanAnual.find({nombre:"tc_"+template.find('#inputNumbrePlan').value,tipo:"trabajoCooperativo"}).count();
        if(planesCount==0&&!template.find('#inputNumbrePlan').value=='') {
            PlanAnual.insert({
                nombre: "tc_"+template.find('#inputNumbrePlan').value,
                autor: [Session.get('logedUser')],
                tipo: "trabajoCooperativo",
                roles: [],
                planCompletado: 40,
                paralelos: [],
                material: [],
                compartido: [],
                observadorPor:[],
                otros:[],
                evaluacionProceso:[],
                docentes:[]

            });
            Session.set('nav-nuevoPlanAnual','');
            Session.setPersistent('op-agregarPlan','');
            Session.setPersistent('op-nuevoPlan','active');

        }
        else{
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El nombre del plan ya existe, ingrese uno nuevo! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
            template.find('#inputNumbrePlan').value='';
        }
    }



    if(Session.get('plan')=='anual') {
        var listaPlanes = PlanAnual.find({tipo:'anual'});
        listaPlanes.forEach(function (post) {
            Session.setPersistent('currentPlan', post);
            Session.setPersistent('currentPlanId',post._id);
            Session.setPersistent('currentPlanNombre',post.nombre);


        });
    }
    if(Session.get('plan')=='elemental') {
        var listaPlanes = PlanAnual.find({tipo:'elemental'});
        listaPlanes.forEach(function (post) {
            Session.setPersistent('currentPlan', post);
            Session.setPersistent('currentPlanId',post._id);
            Session.setPersistent('currentPlanNombre',post.nombre);


        });
    }
    if(Session.get('plan')=='proyecto') {
        var listaPlanes = PlanAnual.find({tipo:'proyecto'});
        listaPlanes.forEach(function (post) {
            Session.setPersistent('currentPlan', post);
            Session.setPersistent('currentPlanId',post._id);
            Session.setPersistent('currentPlanNombre',post.nombre);


        });
    }
    if(Session.get('plan')=='trabajoCooperativo') {
        var listaPlanes = PlanAnual.find({tipo:'trabajoCooperativo'});
        listaPlanes.forEach(function (post) {
            Session.setPersistent('currentPlan', post);
            Session.setPersistent('currentPlanId',post._id);
            Session.setPersistent('currentPlanNombre',post.nombre);


        });
    }


    $('#configuracion').show();

    return false;


},
    'click .EditarPlan':function () {
        Session.set('nav-EditarPlan','open')
    }
});