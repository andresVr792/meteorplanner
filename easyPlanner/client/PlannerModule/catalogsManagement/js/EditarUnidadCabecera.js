
Template.EditarUnidadCabecera.onCreated(function () {
    var self=this;
    self.autorun(function () {

        self.subscribe('numeroUnidad');
        $('#unidadSelectEdt').val(Session.get('currentUnity').numeroUnidad);

    });
});

Template.EditarUnidadCabecera.helpers({

        numeroUnidad:()=>{
             return NumeroUnidad.find();
        },
        semanas:()=>{
            return Session.get('currentUnity').duracionUnidad;
        },
        titulo:()=>{
            return Session.get('currentUnity').titulo;
        },
        numeorUnidadActual:()=>{
            return Session.get('currentUnity').numeroUnidad;
        }


});

Template.EditarUnidadCabecera.events({


    'input #inputNombreUnidad':function (event,template) {
        var titulo=template.find('#inputNombreUnidad').value;
        if(!titulo=='') {
            Meteor.call('actualizarUnidadTitulo', Session.get('currentUnity')._id, titulo);
        }

    },
    'click .close-EditarUnidad':function () {
        return Session.set('nav-editarPlan','');
        sAlert.info('<div class="text-center"><i class="fa fa-window-close"></i> Info!</div> <br> <div class="text-center">La edición de la unidad ha sido cancelada</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

    },
    'input #duracionUnidad':function (event,template) {
        var duracion=template.find('#duracionUnidad').value;

        Meteor.call('actualizarDuracionUnidad',Session.get('currentUnity')._id,duracion);


    },
    'change form#EditarUnidadCabeceraFrm #unidadSelectEdt': function(event,template) {
        var numeroUnidad = template.find('#unidadSelectEdt').value;
        var listUnidades = Unidad.find();

        titulo=template.find('#inputNombreUnidad').value;
        duracion=template.find('#duracionUnidad').value;
         if(!findRepetitions(listUnidades,numeroUnidad)&&titulo!=""&&duracion!="") {
             Meteor.call('actualizarNumeroUnidad', Session.get('currentUnity')._id, numeroUnidad);
             Session.setPersistent('currentUnity',Unidad.findOne({numeroUnidad:parseInt(numeroUnidad)}));
         }
         else{
             sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precausión!</div> <br> <div class="text-center">El número de unidad ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
             template.find('#unidadSelectEdt').value=template.find('#unidadSelectEdt > option:first').value;
         }




    }
});
function findRepetitions(collection,value){
    var a;
    collection.forEach(function (post) {

        if(post.numeroUnidad==value)
            a=true;
    });
    return a;
}