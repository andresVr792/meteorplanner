Template.ObjetivosIntegradoresCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        var subiveltmp=Session.get('currentPlan');
        if(!subiveltmp=='') {
            self.subscribe('objetivosIntegradoresFiltro', Session.get('subnivelTmp'));
        }
    });
});
Template.ObjetivosIntegradoresCatalog.rendered = function () {
    $("#slcObjetivoIntegrador").select2({
        placeholder: "Selecciona/busca un objetivo integrador",
        language: "es"
    });
}

Template.ObjetivosIntegradoresCatalog.helpers({
    objetivos: function () {
        return ObjetivosIntegradores.find();
    }
});
Template.ObjetivosIntegradoresCatalog.events({
        'click .close-ingresarObjetivoIntegrador ': function (event, template){
                Session.set('nav-objetivoIntegrador', '');

template.find('#txtAreaObjetivoIntegrador').textContent="";
        },
'click .btn-AgregarObjetivointegrador': function (event, template) {

    var objTmp = $('#slcObjetivoIntegrador').val();
    var objIntegrador=   PlanAnual.findOne({_id:Session.get('currentPlan')._id}).objetivosIntegradores;
    porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
    if (countCursor(objIntegrador)==0) {
        Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 5);
        Meteor.call('updatePushObjetivosIntegradores', Session.get('currentPlan')._id, objTmp);
        template.find('#txtAreaObjetivoIntegrador').textContent="";
    }
    else {
        if (!findRepetitions(objIntegrador, objTmp) && objTmp != "") {
            Meteor.call('updatePushObjetivosIntegradores', Session.get('currentPlan')._id, objTmp);
            template.find('#txtAreaObjetivoIntegrador').textContent="";
        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El objetivo ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }
    }
    template.find('#txtAreaObjetivoIntegrador').textContent="";

    Session.set('nav-objetivoIntegrador', '');
    return false;



},
'click .btn-verObjetivoIntegrador':function(event, template){
    Session.set('nav-detalle','open');
    var objTmp = $('#slcObjetivoIntegrador').val();
    template.find('#txtAreaObjetivoIntegrador').textContent=objTmp;
    return false;
}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}