Template.NuevaUnidad.onCreated(function () {
    var self=this;
    self.autorun(function () {

        self.subscribe('numeroUnidad');


    });
});

Template.NuevaUnidad.helpers({

        numeroUnidad:()=>{
        return NumeroUnidad.find();
        },
        isElemental:()=>{
        return Session.get('plan') =='elemental';
        }

});
Template.NuevaUnidad.rendered=function () {
    $('#fechaInicio').datepicker();
    $('#fechaFin').datepicker();
}
Template.NuevaUnidad.events({

'click .close-nuevaUnidad':function (event,template) {


    Session.set('nav-nuevaUnidad','');
    $('#unidadSelect').val($('#unidadSelect > option:first').val());
    template.find('#duracionUnidad').value=null;
    template.find('#inputNombreUnidad').value=null;
   if(Session.get('plan')=='elemental') {
       template.find('#fechaInicio').value = null;
       template.find('#fechaFin').value = null;
       template.find('#elementoIntegrador').value = null;
       template.find('#experienciaAprendizaje').value = null;
   }
    return false;


},
'click .btn-NuevaUnidad':function (event, template) {
    numeroDeUnidad=template.find('.slctUnidad :selected').value;
    unidadesPlan=Unidad.find();
    titulo=template.find('#inputNombreUnidad').value;
    duracion=template.find('#duracionUnidad').value;
    if(Session.get('plan')=='elemental'){
        fechaInicio=template.find('#fechaInicio').value;
        fechaFin=template.find('#fechaFin').value;
        elementoIntegrador=template.find('#elementoIntegrador').value;
        experienciaAprendizaje=template.find('#experienciaAprendizaje').value;
    }

    if(!findRepetitions(unidadesPlan,numeroDeUnidad)&&titulo!=""&&duracion!="") {
        if(Session.get('plan')=='anual') {

            var tiempototalIngresado=tiempoTotal(Unidad.find({idPlanAnual:Session.get('currentPlan')._id}));
            if((parseInt(tiempototalIngresado)+parseInt(duracion))<=34) {

                Meteor.call('iniciarlizarUnidad', Session.get('currentPlan')._id, numeroDeUnidad, titulo, duracion);
                Session.setPersistent('currentUnity', Unidad.findOne({numeroUnidad: parseInt(numeroDeUnidad)}));
                Session.set('nav-nuevaUnidad', 'active');



            }
            else{
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El tiempo de las unidades no debe ser mayor que 34! </div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
                Session.set('nav-nuevaUnidad','');
            }
            $('#unidadSelect').val($('#unidadSelect > option:first').val());
            template.find('#duracionUnidad').value = null;
            template.find('#inputNombreUnidad').value = null;

        }
        else {
            var tiempototalIngresado=tiempoTotal(Unidad.find({idPlanAnual:Session.get('currentPlan')._id}));
            if((parseInt(tiempototalIngresado)+parseInt(duracion))<=34) {

                Meteor.call('iniciarlizarUnidadElemental', Session.get('currentPlan')._id, numeroDeUnidad, titulo, duracion, fechaInicio, fechaFin, elementoIntegrador, experienciaAprendizaje);
                Session.setPersistent('currentUnity', Unidad.findOne({numeroUnidad: parseInt(numeroDeUnidad)}));
                Session.set('nav-nuevaUnidad', 'active');


            }else {
                sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El tiempo de las unidades no debe ser mayor que 34! </div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});
                Session.set('nav-nuevaUnidad','open');
            }
            $('#unidadSelect').val($('#unidadSelect > option:first').val());
            template.find('#duracionUnidad').value = null;
            template.find('#inputNombreUnidad').value = null;

            template.find('#fechaInicio').value = null;
            template.find('#fechaFin').value = null;
            template.find('#elementoIntegrador').value = null;
            template.find('#experienciaAprendizaje').value = null;
        }
    }
    else{
        sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precausión!</div> <br> <div class="text-center">El número de unidad ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

    }



    return false;
}
});
function findRepetitions(collection,value){
    var a;
    collection.forEach(function (post) {

        if(post.numeroUnidad==value)
            a=true;
    });
    return a;
}
function tiempoTotal(collection){
    var suma=0;
    collection.forEach(function(doc){
        suma=suma+doc.duracionUnidad;
    });
    return suma;
}