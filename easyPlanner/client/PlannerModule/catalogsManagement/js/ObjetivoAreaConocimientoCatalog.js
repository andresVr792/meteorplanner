import '../../../../public/plugins/select2/select2.full.min.js';
Template.ObjetivosAreaConocimientoCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        currentPlanOACC=Session.get('currentPlan');
        if(!currentPlanOACC=='') {
            self.subscribe('areasConocimiento');
            var plan = PlanAnual.findOne({_id:currentPlanOACC._id});

            var area=Areas.findOne({value:plan.areaConocimiento});
            var asignatura=Asignaturas.findOne({value:plan.asignatura});
            self.subscribe('objetivosGeneralesFiltrados',area);
        }
    });
});
Template.ObjetivosAreaConocimientoCatalog.rendered = function () {
    $("#slcObjetivoGeneral").select2({
        placeholder: "Selecciona/busca objetivo general",
        language: "es"

    });
}

Template.ObjetivosAreaConocimientoCatalog.helpers({
    objetivos: function () {
        return ObjetivosGenerales.find();
    },
    navDetalle:function () {
        return Session.get('nav-detalle')=='open';
    }
});
Template.ObjetivosAreaConocimientoCatalog.events({
        'click .close-ingresarObjetivoGeneral ': function(event,template) {
            Session.set('nav-objetivoGeneral', '');
            template.find('#txtAreaObjetivo').textContent=null;
            Session.set('nav-detalle','');

        },
        'click .btn-AgregarObjetivo': function (event, template) {

            var objTmp = $('#slcObjetivoGeneral').val();
            var objGeneral=   PlanAnual.findOne({_id:Session.get('currentPlan')._id}).objetivosAreaConocimiento;
            porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
            if (countCursor(objGeneral)==0) {
                Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 5);
                Meteor.call('updatePushObjetivosGenerales', Session.get('currentPlan')._id, objTmp);

            }
            else {
                if (!findRepetitions(objGeneral, objTmp) && objTmp != "") {
                    Meteor.call('updatePushObjetivosGenerales', Session.get('currentPlan')._id, objTmp);

                }
                else {
                    sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El objetivo ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                        effect: 'bouncyflip',
                        position: 'top-right',
                        timeout: 3000,
                        onRouteClose: false,
                        stack: false,
                        html: true,
                        offset: '50px'
                    });

                }
            }
                Session.set('nav-objetivoGeneral', '');
                Session.set("nav-detalle",'');
                template.find('#txtAreaObjetivo').textContent=null;
                return false;



        },
        'click .btn-verObjetivo':function(event, template){
            Session.set('nav-detalle','open');
             var objTmp = $('#slcObjetivoGeneral').val();
            template.find('#txtAreaObjetivo').textContent=objTmp;
             return false;
        }
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}