Template.AdaptacionesCurricularesCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {

        self.subscribe("neeData");
        self.subscribe("diacData")
    });
});

Template.AdaptacionesCurricularesCatalog.rendered = function () {
    $("#slcNEE").select2({
        placeholder: "Selecciona/busca la NEE",
        language: "es"


    });
    $("#slcDIAC").select2({
        placeholder: "Selecciona/busca el DIAC",
        language: "es"

    });
}


Template.AdaptacionesCurricularesCatalog.helpers({
    neeOp: function () {
        return CategoriaNEE.find({tipo:Session.get('tipoNEE')});
    },
    especificacionNEE:function(){
        return NeeData.find({tipo:Session.get('tipoNEE')});
    },
    diac:function(){
        return DiacData.find({grado:Session.get('tipoDIAC')});
    }

});
Template.AdaptacionesCurricularesCatalog.events({
        'click .close-acurriculares ': function(event,template) {
        Session.set('nav-acurriculares', '');
        $('#tipoNEE').val($('#slcParalelo > option:first').val());
        $('#tipoDIAC').val($('#slcParalelo > option:first').val());

},

'change form#IngresarAdaptacionesCurricularesFrm #tipoNEE': function(event,template) {
    var tipo = template.find('#tipoNEE').value;

    Session.set('tipoNEE',tipo);

},
'change form#IngresarAdaptacionesCurricularesFrm #tipoDIAC': function(event,template) {
    var tipo = template.find('#tipoDIAC').value;

    Session.set('tipoDIAC',parseInt(tipo));

},
'click .btn-AgregarEspecificacion': function (event, template) {


    var nee = template.find('#slcNEE').value;
    var diac = template.find('#slcDIAC').value;
    var alumno=template.find('input[name=alumnoAdaptacion]').value;
    var diagnostico=template.find('#textAreaDiagnostico').value;
    var adaptacionesCurriculares=   Unidad.findOne({_id:Session.get('currentUnity')._id}).adaptacionesCurriculares;
    if(!findRepetitions(adaptacionesCurriculares,nee)) {
        Meteor.call('actualizarAdaptacionCurricularPush',Session.get('currentUnity'),nee,diac,alumno,diagnostico);

    }
    else{
        sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El contenido ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

    }
    template.find('input[name=alumnoAdaptacion]').value='';
    Session.set('nav-acurriculares', '');

    return false;



}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.nee==value;
    });

}