Template.ContenidosCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        currentPCC=Session.get('currentPlan');
        if(!currentPCC=='') {
            var plan = PlanAnual.findOne({_id: currentPCC._id});
            var areaObj = Areas.findOne({value: plan.areaConocimiento});
            Session.setPersistent('areaObjTmp', areaObj);

            if(!Session.get('areaObjTmp')=='') {
                if (Session.get('areaObjTmp').prefix == 'EF') {

                    self.subscribe('contenidosConceptualesFiltroEF', Session.get('areaObjTmp'), Session.get('asignaturaTmp'));
                }
                if (Session.get('areaObjTmp').prefix != 'EF') {
                    self.subscribe('contenidosConceptualesFiltro', Session.get('areaObjTmp'), Session.get('asignaturaTmp'), Session.get('subnivelTmp'));
                }
            }
        }
    });
});

Template.ContenidosCatalog.rendered = function () {
    $("#slcContenidosConceptuales").select2({
        placeholder: "Selecciona/busca el contenido",
        language: "es",
        tags:true

    });
}


Template.ContenidosCatalog.helpers({
    contenidos: function () {
        return Contenidos.find();
    }
});
Template.ContenidosCatalog.events({
        'click .close-contenidos ': function(events,template)  {
            template.find('#txtAreaDetalleContenido').value="";
            template.find('#tiempoContenidosConceptuales').value="";
        Session.set('nav-contenidos', '');

},
'click .btn-AgregarContenidos': function (event, template) {

    var producto = template.find('#txtAreaDetalleContenido').value;
    var contenidoTmp = template.find('#slcContenidosConceptuales').value;
    var tiempoTmp = template.find('#tiempoContenidosConceptuales').value;
    var tiempoUnidad=Session.get('currentUnity').duracionUnidad;
    var tiempototalIngresado=tiempoTotal(ContenidosConceptualesUnidad.find({idUnidad:Session.get('currentUnityId')}));
    var contenidos=   ContenidosConceptualesUnidad.find({idUnidad:Session.get('currentUnityId'),texto:contenidoTmp}).count();

    if((parseInt(tiempototalIngresado)+parseInt(tiempoTmp))<=parseInt(tiempoUnidad)) {
        if (contenidos == 0 && contenidoTmp != '' && producto != '') {

            Meteor.call('iniciarlizarContenido', Session.get('currentPlan'), Session.get('currentUnity'), contenidoTmp, producto, tiempoTmp);
            Meteor.call('actualizarContenidoPush', Session.get('currentUnity'), contenidoTmp);
            Session.set('sec-contenidosConceptules', 'active');
            Session.set('currentContenidoConceptual', ContenidosConceptualesUnidad.findOne({
                texto: contenidoTmp,
                idUnidad:Session.get('currentUnity')._id
            }));

        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El contenido ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }
    }
    else{
        sAlert.error('<div class="text-center"><i class="fa fa-window-close"></i> Error!</div> <br> <div class="text-center">Tiempo limite de unidad excedido! <br> No se permite que la suma de los tiempos de los contenidos conceptuales sea mayor que el de la unidad</div>', {
            effect: 'bouncyflip',
            position: 'top-right',
            timeout: 5000,
            onRouteClose: false,
            stack: false,
            html: true,
            offset: '50px'
        });

    }
    template.find('#txtAreaDetalleContenido').value="";
    template.find('#tiempoContenidosConceptuales').value="";
    Session.set('nav-contenidos', '');
    return false;



}

});

function tiempoTotal(collection){
    var suma=0;
    collection.forEach(function(doc){
      suma=suma+doc.duracionContenido;
    });
    return suma;
}

