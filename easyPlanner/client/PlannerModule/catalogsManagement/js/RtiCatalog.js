Template.RTICatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('allTecnicas');
        self.subscribe('allInstrumentos');
        if (Session.get('nav-rti-edit') == 'open') {
            Session.set('nav-rti', 'open');
            $('#tecnicas').val(Session.get('tecnicasRTI'));
            $('#instrumentos').val(Session.get('instrumentoRTI'));
        } else {
            $('#tecnicas').val('');
            $('#instrumentos').val('');
        }
    });

});

Template.RTICatalog.rendered = function () {
    $("#slcTecnicas").select2({
        placeholder: "Selecciona/busca la técnnica",
        language: "es",
        tags:true,
        allowClear:true


    });
    $("#slcInstrumentos").select2({
        placeholder: "Selecciona/busca el instrumento",
        language: "es",
        tags:true,
        allowClear:true


    });
}

Template.RTICatalog.helpers({
    tecnicas: function () {
        return Tecnicas.find();
    },
    instrumentos:function(){
        return Instrumentos.find();
    }
});
Template.RTICatalog.events({
        'click .close-ingresarRTi ': () => {
        Session.set('nav-rti', '');

},
'click .btn-AgregarRti': function (event, template) {

    if (Session.get('nav-rti-edit') == 'open') {
        var tecnicatmp = template.find('#slcTecnicas').value;
        var instrumentostmp = template.find('#slcInstrumentos').value;
        Meteor.call('actualizarRTIPull',Session.get('currentContenidoConceptual'),Session.get('tecnicasRTI'),Session.get('instrumentoRTI'));
        Meteor.call('actualizarRTIPush', Session.get('currentContenidoConceptual'), tecnicatmp, instrumentostmp);

    }
    else {
        var tecnicatmp = template.find('#slcTecnicas').value;
        var instrumentostmp = template.find('#slcInstrumentos').value;
        var tecnicasCC = ContenidosConceptualesUnidad.findOne({_id: Session.get('currentContenidoConceptual')._id}).rtis;
        if (!findRepetitions(tecnicasCC, tecnicatmp) && tecnicatmp != "" && instrumentostmp != "") {
            Meteor.call('actualizarRTIPush', Session.get('currentContenidoConceptual'), tecnicatmp, instrumentostmp);

        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El indicador ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }
    }
    Session.set('nav-rti', '');

    return false;



}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}