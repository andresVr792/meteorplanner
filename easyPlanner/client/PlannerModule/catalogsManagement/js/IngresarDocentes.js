import '../../../../public/plugins/select2/i18n/es';
import '../../../../public/plugins/select2/select2.full.min.js';

Template.IngresarDocente.onCreated(function () {
    this.autorun(() => {
        this.subscribe('filterUser','educador');
    this.subscribe('docentesAnual');
})
    ;
});


Template.IngresarDocente.helpers({
    docentes: function () {
        return Meteor.users.find({},{sort: {"profile.lastName": 1}});
    }
});
Template.IngresarDocente.events({
        'click .close-ingresarDocente ': () => {
            Session.set('nav-docente', '');

},
        'click .btn-Agregar': function (event, template) {

    //var docenteTmp = $('.inDocente').val();
    var docenteTmp=$('#slcDocente').val();

    docentes = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).docentes;
    porcentajePlanCompleto = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).planCompletado;
    var docentesList = PlanAnual.findOne({_id: Session.get('currentPlan')._id}).docentes;

    if (countCursor(docentes)==0) {
        Meteor.call('updatePlanCompletado', Session.get('currentPlan')._id, porcentajePlanCompleto + 5);
        Meteor.call('updatePushDocente', Session.get('currentPlan')._id, docenteTmp);


    } else {

        if (!findRepetitions(docentesList, docenteTmp) && docenteTmp != "") {
            Meteor.call('updatePushDocente', Session.get('currentPlan')._id, docenteTmp);

        }
        else {
            sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El docente ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {
                effect: 'bouncyflip',
                position: 'top-right',
                timeout: 3000,
                onRouteClose: false,
                stack: false,
                html: true,
                offset: '50px'
            });

        }
    }
        Session.set('nav-docente', '');
    return false;

}
});
function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.nombre==value;
    });

}
function countCursor(col){
    cont=0
    col.forEach(function (doc) {
        cont++;
    });
    return cont;
}