import moment from 'moment';
Template.EditarPlanAnual.onCreated(function () {
    this.autorun(() => {
    if(Session.get('plan')=="anual")
    {
        this.subscribe('compartidoAnual', Meteor.userId(), Session.get('logedUser'));
    }
    if(Session.get('plan')=="elemental"){
        this.subscribe('compartidoElemental', Meteor.userId(), Session.get('logedUser'));

    }
    if(Session.get('plan')=="proyecto"){
        this.subscribe('compartidoProyecto', Meteor.userId(), Session.get('logedUser'));

    }

    if(Session.get('plan')=="trabajoCooperativo"){
        this.subscribe('compartidoTrabajoCooperativo', Meteor.userId(), Session.get('logedUser'));

    }

})
    ;
});

Template.EditarPlanAnual.helpers({
        ocultarNuevoPlanFrm: function () {
            return Session.get('nav-nuevoPlanAnual') === '';
        },
        compartirActivado:function () {
            return Session.get('compartir')!=null;
        },
        userPlans: function()  {

            if(Session.get('plan')=="anual"&& Session.get('compartir')== null)
                return PlanAnual.find({tipo:"anual","autor.idDocente":Meteor.userId()});
            if(Session.get('plan')=="elemental" && Session.get('compartir')== null)
                return PlanAnual.find({tipo:"elemental","autor.idDocente":Meteor.userId()});
            if(Session.get('plan')=='proyecto'&& Session.get('compartir')== null)
                return PlanAnual.find({tipo:"proyecto","autor.idDocente":Meteor.userId()});
            if(Session.get('plan')=='trabajoCooperativo'&& Session.get('compartir')== null)
                return PlanAnual.find({tipo:"trabajoCooperativo","autor.idDocente":Meteor.userId()});
            if(Session.get('plan')=="anual" || Session.get('compartir')=="compartidoAnual") {
                return PlanAnual.find({
                    compartido: [
                        {
                            idDocente: Meteor.userId(),
                            correoElectronico: Session.get('logedUser').emails[0].address
                        }
                    ], tipo: "anual"

                });
            }
            if(Session.get('plan')=="elemental"||Session.get('compartir')=="compartidoElemental") {
                return PlanAnual.find({
                    compartido: [
                        {
                            idDocente: Meteor.userId(),
                            correoElectronico: Session.get('logedUser').emails[0].address
                        }
                    ]
                    , tipo: "elemental"
                });
            }
            if(Session.get('plan')=="proyecto"||Session.get('compartir')=="compartidoProyecto") {
                return PlanAnual.find({
                    compartido: [
                        {
                            idDocente: Meteor.userId(),
                            correoElectronico: Session.get('logedUser').emails[0].address
                        }
                    ]
                    , tipo: "proyecto"
                });
            }
            if(Session.get('plan')=="trabajoCooperativo"||Session.get('compartir')=="compartidoTrabajoCooperativo") {
                return PlanAnual.find({
                    compartido: [
                        {
                            idDocente: Meteor.userId(),
                            correoElectronico: Session.get('logedUser').emails[0].address
                        }
                    ]
                    , tipo: "trabajoCooperativo"
                });
            }
},
dateFormat:function () {
    return moment(this.fechaCreacion).format('MM/D/YYYY | HH:MM');
}
})
;

Template.EditarPlanAnual.events({

    'click .editarPlan': function (event, template) {


        Session.setPersistent('nav-nuevoPlanAnual', '');
        Session.set('op-editarPlan', 'active');
        Session.set('nav-planificarUnidadAnual','');

        Session.setPersistent('currentPlan', this);
        Session.setPersistent('currentPlanNombre', this.nombre);
        Session.setPersistent('currentPlanId', this._id);

        $('#configuracion').show();
        return false;


    },
    'click .eliminarPlan':function(){
        Meteor.call('deletePlanAnual',this._id);
    }

});