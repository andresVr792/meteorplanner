Template.DestrezasCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('destrezasFiltro',Session.get('areaObjTmp'),Session.get('asignaturaTmp'),Session.get('subnivelTmp'));

    });
});
Template.DestrezasCatalog.rendered = function () {
    $("#slcDestrezas").select2({
        placeholder: "Selecciona/busca la destreza",
        language: "es"

    });
}

Template.DestrezasCatalog.helpers({
    destrezas: function () {
        return Destrezas.find();
    }
});
Template.DestrezasCatalog.events({
        'click .close-destrezas ': function (event,template) {
        Session.set('nav-destrezas', '');
        template.find('#txtAreaDestreza').textContent='';


},
'click .btn-AgregarDestreza': function (event, template) {

    //var objTmp = $('input[name=objetivoGeneral]').val();
    var destrezaTmp = template.find('#slcDestrezas').value;
    var destrezas=   Unidad.findOne({_id:Session.get('currentUnity')._id}).destrezas;
    if(!findRepetitions(destrezas,destrezaTmp)&&destrezaTmp!="") {
        Meteor.call('actualizarDestrezaPush',Session.get('currentUnity')._id,destrezaTmp);

    }
    else{
        sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">La destreza ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

    }

    template.find('#txtAreaDestreza').textContent='';
    Session.set('nav-destrezas', '');
    return false;



},
'click .btn-verDestreza':function(event, template){
    Session.set('nav-detalle','open');
    var objTmp = $('#slcDestrezas').val();
    template.find('#txtAreaDestreza').textContent=objTmp;
    return false;
}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}