Template.CriteriosCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('criteriosFiltro',Session.get('areaObjTmp'),Session.get('subnivelTmp'));

    });
});


Template.CriteriosCatalog.helpers({
    criterios: function () {
        return CriteriosEvaluacion.find();
    }
});
Template.CriteriosCatalog.events({
        'click .close-ingresarCriterios ': () => {
        Session.set('nav-criterios', '');

},
'click .btn-AgregarCriterio': function (event, template) {

    //var objTmp = $('input[name=objetivoGeneral]').val();
    var criterioTmp = template.find('input[name=criterio]').value;
    var criterios=   Unidad.findOne({_id:Session.get('currentUnity')._id}).criteriosEvaluacion;
    if(!findRepetitions(criterios,criterioTmp)&&criterioTmp!="") {
        Meteor.call('actualizarCriteriosPush',Session.get('currentUnity')._id,criterioTmp);

    }
    else{
        sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El criterio de evalaución ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

    }
    template.find('#criterio').value="";
    Session.set('nav-criterios', '');

    return false;



},
'click .btn-verCriterioEvaluacion':function(event, template){
    Session.set('nav-detalle','open');
    var objTmp = $('input[name=criterio]').val();
    template.find('#txtAreaCriterioEvaluacion').textContent=objTmp;
    return false;
}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}