
Template.CurriculoIntegradorCatalog.onCreated(function(){
    var self=this;
    self.autorun(function (event,template) {
        // self.subscribe('asignaturas',Session.get('logedUser').profile.areaConocimiento);
        self.subscribe('curriculoIntegrador');
        self.subscribe('ejes');
    });
});
Template.CurriculoIntegradorCatalog.helpers({
    ejesDesarrollo:function(){
        return EjesDesarrolloElemental.find();
    },
    ambitoDesarrollo:function () {
        return CurriculoIntegradorElemental.find({ejes:Session.get('EjeDesarrollo')});


    }


});
Template.CurriculoIntegradorCatalog.events({
    'change form#IngresarCurriculoFrm #ejesDesarrollo':function (event,template) {
        var eje=template.find('#ejesDesarrollo').value;
        Session.set("EjeDesarrollo",eje);
        Meteor.call('updateEjesDesarrolloAprendizaje',Session.get('currentPlan')._id,eje)

    },
    'change form#IngresarCurriculoFrm #ambitosDesarrollo':function (event,template) {
        var ambito=template.find('#ambitosDesarrollo').value;
        Meteor.call('updateAmbitosDesarrolloAprendizaje',Session.get('currentPlan')._id,ambito)
    },
    'click .btnCurriculo':function () {
        Session.set('nav-curriculoIntegrador','open');

        return false;
    },
    'click .close-curriculoIntegrador':function () {
        Session.set('nav-curriculoIntegrador','');

        return false;
    },
    'click .btn-AgregarCurriculo':function (event, template) {
        var eje=template.find('#ejesDesarrollo').value;
        var ambito=template.find('#ambitosDesarrollo').value;
        Meteor.call('updatePushCurriculoIntegrador',Session.get('currentPlan'),eje, ambito);
        Session.set('nav-curriculoIntegrador','');

        return false;

    }
});