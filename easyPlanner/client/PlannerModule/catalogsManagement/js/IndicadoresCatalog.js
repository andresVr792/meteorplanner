Template.IndicadoresCatalog.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('indicadoresFiltro',Session.get('areaObjTmp'),Session.get('asignaturaTmp'),Session.get('subnivelTmp'));
        self.subscribe('criterioFilter',Session.get('areaObjTmp'),Session.get('subnivelTmp'));

    });
});

Template.IndicadoresCatalog.rendered = function () {
    $("#slcIndicadores").select2({
        placeholder: "Selecciona/busca el indicadores",
        language: "es"

    });
}


Template.IndicadoresCatalog.helpers({
    indicadores: function () {
        if(Session.get('tipoEvaluacion')=='criterio')
        return CriteriosEvaluacion.find();
        if(Session.get('tipoEvaluacion')=='indicador')
        return IndicadoresEvaluacion.find();
    }
});
Template.IndicadoresCatalog.events({
        'click .close-ingresarIndicadores ': function(event,template) {
            Session.set('nav-indicadores', '');
            template.find('#txtAreaIndicadores').textContent = '';
            $('#indicadores').val($('#indicadores > option:first').val());
        }

,
'click .btn-AgregarIndicador': function(event, template) {


    var indicadorTmp = template.find('#slcIndicadores').value;
    var inicadores=   Unidad.findOne({_id:Session.get('currentUnity')._id}).indicadores;
    if(!findRepetitions(inicadores,indicadorTmp)&&indicadorTmp!="") {
        Meteor.call('actualizarcriteriosindicadoresEvaluacionPush',Session.get('currentUnity')._id,indicadorTmp);

    }
    else{
        sAlert.warning('<div class="text-center"><i class="fa fa-window-close"></i> Precaución!</div> <br> <div class="text-center">El indicador ya se encuentra en la lista! <br> No se permiten campos vacíos</div>', {effect: 'bouncyflip', position: 'top-right', timeout: 3000, onRouteClose: false, stack: false,html:true, offset: '50px'});

    }

    Session.set('nav-indicadores', '');
    template.find('#txtAreaIndicadores').textContent='';
    return false;



},
'click .btn-verIndicadores':function(event, template){
    Session.set('nav-detalle','open');
    var objTmp = $('#slcIndicadores').val();
    template.find('#txtAreaIndicadores').textContent=objTmp;
    return false;
},
'change form#IngresarIndicadoresFrm #slcSeleccionarIndicadorCriterio': function(event,template){
    var tipoTmp=template.find('#slcSeleccionarIndicadorCriterio').value;
    /*
    El tipoEvaluacion es para definir si es indicador o criterio de avaluación
     */
    Session.setPersistent('tipoEvaluacion',tipoTmp);

}
});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}