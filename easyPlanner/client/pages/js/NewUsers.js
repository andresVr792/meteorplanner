Template.NewUser.onCreated(function(){
    var self=this;
    self.autorun(function () {
        self.subscribe('allActividades');
        if( Session.get('nav-actividades-edit')=='open'){
            Session.set('nav-actividades','open');

            $('#actividades').val( Session.get('actividadEdit'));
            $('#txtAreaDetalleActividad').val( Session.get('actividadDetalleEdit'));
        }else{
            $('#actividades').val('');
            $('#txtAreaDetalleActividad').val( '');
        }

    });
});
Template.NewUser.rendered = function () {
    $("#slcActividades").select2({
        placeholder: "Selecciona/busca la actividad",
        language: "es",
        tags:true,
        allowClear:true


    });
}

Template.NewUser.helpers({
    actividades: function () {
        return Actividades.find();
    }

});
Template.NewUser.events({
        'click .close-newUser ': () => {
        Session.set('nav-newUser', '');




},
'submit form': function (event, template) {

    var primerNombre = template.find('#primerNombre').value;
    var segundoNombre = template.find('#segundoNombre').value;
    var primerApellido = template.find('#primerApellido').value;
    var segundoApellido = template.find('#segundoApellido').value;
    var correo = template.find('#correoElectronico').value;
    var contrasenia = template.find('#contrasenia').value;
    var subnivel = template.find('#slcSubnivel').value;
    var areaC = template.find('#slcAreas').value;


    // console.log(`id:${id} name:${name} cedula: ${cedula}`);
    Meteor.call('crearUsuario', correo, primerApellido,segundoApellido,primerNombre, segundoNombre,areaC, contrasenia,subnivel, (error, result) => {
        console.log(result);
    const user_id = result;

});
    Session.set('nav-newUser', '');
},

});

function findRepetitions(collection,value){
    return _.some(collection,function(valor){
        return valor.texto==value;
    });

}