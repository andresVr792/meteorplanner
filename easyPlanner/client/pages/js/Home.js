Template.Home.onCreated(function () {
    let settings = 'pjs-settings.json';
    this.autorun(()=> {
        if (particlesJS) {
            console.log(`loading particles.js config from "${settings}"`)
            // window.particlesJS.load = function(tag_id, path_config_json, callback)
            particlesJS.load('particles-js', settings, function () {
                console.log('callback - particles.js config loaded');
            });
        }
        AccountsTemplates.logout();
    });
});