import { Meteor }          from 'meteor/meteor';
import { FilesCollection } from 'meteor/ostrio:files';

const Images = new FilesCollection({
    debug: true,
    collectionName: 'Images',
    public:true,
    downloadRoute:'/resources',
    storagePath:'/Users/andresvr/Documents/Coana/meteorplanner/easyPlanner/public/resources',
    permissions:0777,
    parentDirPermissions: 0777,
    allowClientCode: true, // Disallow remove files from Client
    onBeforeUpload: function (file) {
        // Allow upload files under 10MB, and only in png/jpg/jpeg formats
        if (file.size <= 1024 * 1024 * 25 ) {
            return true;
        }
        return 'El archivo debe tener un peso menor de 25MB';
    }
});

/*if (Meteor.isServer) {
    Images.denyClient();
    Meteor.publish('files.images.all', function () {
        return Images.find().cursor;
    });
} else {
    Meteor.subscribe('files.images.all');
}*/

export default Images;
