if(Meteor.isClient) {
    Accounts.onLogin(function () {
            FlowRouter.go('dashboard');
        }
    );
    Accounts.onLogout(function () {
            FlowRouter.go('home');
        }
    );
}

// Home Page
FlowRouter.route('/', {
    name: 'home',
    action() {
        BlazeLayout.render("HomeLayout", {main: "Home"});
    }
});

FlowRouter.route('/controlActividades', {
    name: 'controlActividades',
    action() {
        BlazeLayout.render("AppLayout", {main: "ControlActividades"});
    }
});

FlowRouter.route('/dashboardResume', {
    name: 'dashboardResume',
    action() {
        BlazeLayout.render("AppLayout", {main: "DashboardResume"});
    }
});


// Home Dashboard
FlowRouter.route('/dashboard', {
    name: 'dashboard',
    action() {
        BlazeLayout.render("AppLayout", {main: "Dashboard"});
    }
});

// user account

var adminRoutes=FlowRouter.group({
    prefix: '/admin',
    name: 'admin'
});

adminRoutes.route('/users', {
    name:'users',
    action() {
        BlazeLayout.render("AppLayout", {main: "Users"});
    }
});
//planES
var educatorRoutes=FlowRouter.group({
    prefix: '/educador',
    name: 'educador'
});
educatorRoutes.route('/planAnual', {
    name:'planAnual',
    action() {
        BlazeLayout.render("AppLayout", {main: "PlanificacionAnual"});
    }
});
educatorRoutes.route('/planAnualUnidad', {
    name:'planAnualUnidad',
    action() {
        BlazeLayout.render("AppLayout", {main: "PlanificacionUnidadAnual"});
    }
});
educatorRoutes.route('/planElementalUnidad', {
    name:'planElementalUnidad',
    action() {
        BlazeLayout.render("AppLayout", {main: "PlanificacionUnidadAnual"});
    }
});
educatorRoutes.route('/planElemental', {
    name:'planElemental',
    action() {
        BlazeLayout.render("AppLayout", {main: "PlanificacionElemental"});
    }
});
educatorRoutes.route('/reportePrueba', {
    name:'reportePrueba',
    action() {
        BlazeLayout.render("AppLayout", {main: "Report"});
    }
});

educatorRoutes.route('/reporteAnualParametros/:currentPlanId/:user', {
    name: 'reporteAnualParametros',
    action: function(params, queryParams) {
        BlazeLayout.render("ReportLayout", {
            main: "ReportePlanAnual",
            currentPlanId:params.currentPlanId,
            user:params.user

        });
    }
});
educatorRoutes.route('/reporteMicroParametros/:currentPlanId/:currentUnityId', {
    name: 'reporteMicroParametros',
    action: function(params, queryParams) {
        BlazeLayout.render("ReportLayout", {
            main: "ReportePlanMicro",
            currentPlanId:params.currentPlanId,
            currentUnityId:params.currentUnityId

        });
    }
});
educatorRoutes.route('/reportePlanPrimero/:currentPlanId/:currentUnityId', {
    name: 'reportePlanPrimero',
    action: function(params, queryParams) {
        BlazeLayout.render("ReportLayout", {
            main: "ReportePlanPrimero",
            currentPlanId:params.currentPlanId,
            currentUnityId:params.currentUnityId

        });
    }
});
//proyectos
var proyectosRoutes=FlowRouter.group({
    prefix: '/proyectos',
    name: 'proyectos'
});
proyectosRoutes.route('/nuevoProyecto', {
    name:'nuevoProyecto',
    action() {
        BlazeLayout.render("AppLayout", {main: "Project"});
    }
});
//trabajo cooperativo
var trabajoCooperativoRoutes=FlowRouter.group({
    prefix: '/cooperativo',
    name: 'cooperativo'
});
trabajoCooperativoRoutes.route('/trabajoCooperativo', {
    name:'trabajoCooperativo',
    action() {
        BlazeLayout.render("AppLayout", {main: "TrabajoCooperativo"});
    }
});