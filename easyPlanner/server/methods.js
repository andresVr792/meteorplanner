const path = require('path');
const childProcess = require('child_process');
const phantomjs = require('phantomjs-prebuilt');

Meteor.methods({
    toggleAdmin(id){
        if (Roles.userIsInRole(id, 'admin')) {
            Roles.removeUsersFromRoles(id, 'admin');
        }
        else {
            Roles.addUsersToRoles(id, 'admin');
        }
    },
    toggleEducador(id){
        if (Roles.userIsInRole(id, 'educador')) {
            Roles.removeUsersFromRoles(id, 'educador');
        }
        else {
            Roles.addUsersToRoles(id, 'educador');
        }
    },
    toggleEducadorEL(id){
        if (Roles.userIsInRole(id, 'educadorEL')) {
            Roles.removeUsersFromRoles(id, 'educadorEL');
        }
        else {
            Roles.addUsersToRoles(id, 'educadorEL');
        }
    },
    toggleCoordinador(id){
        if (Roles.userIsInRole(id, 'coordinador')) {
            Roles.removeUsersFromRoles(id, 'coordinador');
        }
        else {
            Roles.addUsersToRoles(id, 'coordinador');
        }
    },
    toggleJefeArea(id){
        if (Roles.userIsInRole(id, 'jefeArea')) {
            Roles.removeUsersFromRoles(id, 'jefeArea');
        }
        else {
            Roles.addUsersToRoles(id, 'jefeArea');
        }
    },
    toggleDeshabilitado(id){
        if (Roles.userIsInRole(id, 'deshabilitado')) {
            Roles.removeUsersFromRoles(id, 'deshabilitado');
        }
        else {
            Roles.addUsersToRoles(id, 'deshabilitado');
        }
    },
    sessionUser(){
        return Meteor.users.find({});
    },
    reviewRepetitions(collection,value){
        return _.some(collection, function (valor) {
            return valor.texto == value;
        });
    },
    phantom(page, filename,userId,idPlan) {
        const binPath = phantomjs.path;
        console.log(binPath);
        const projectPath = path.resolve('../../../../../.');
        var nombre=PlanAnual.findOne({_id:idPlan});
//        if(!ReportRoutes.find({ruta:`http://localhost:3000/${filename}${nombre.nombre}.pdf`})){
        ReportRoutes.remove({  nombre: filename+nombre.nombre+'.pdf'});
        ReportRoutes.insert({
            nombre: filename+nombre.nombre+'.pdf',
            nombrePlan:nombre.nombre,
            ruta:`http://181.39.150.74:3000/${filename}${nombre.nombre}.pdf`,
            idAutor: userId,
            tipo: "anual"

        });//}

        const childArgs = [
            path.join(`${projectPath}/public/pdf.js`),
            page,
            `${projectPath}/public/${filename}${nombre.nombre}.pdf`,
            'A4',
        ];
        console.log(childArgs);

        childProcess.execFile(binPath, childArgs, (err, stdout, stderr) => {
            console.log('phantomresultados:');
        console.log(filename);
        console.log(`err:${err}`);
        console.log(`stdout:${stdout}`);
        console.log(`stderr:${stderr}`);


    });
    },
    sendEmail: function (to, from, subject, text,filename,filepath) {
        check([to, from, subject, text], [String]);
        // Let other method calls from the same client start running,
        // without waiting for the email sending to complete.
        this.unblock();
        var cid_value = Date.now() + filename;
        var attachment = {
            fileName: filename,
            filePath: filepath,
         };
        Email.send({
            to: to,
            from: from,
            subject: subject,
            text: text,
            attachments:[{
                fileName: filename,
                filePath: filepath,
            }]
        });

    },








});
