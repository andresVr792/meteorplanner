Meteor.publish('allUsers',function () {
    if(Roles.userIsInRole(this.userId,'admin')) {
        return Meteor.users.find({});
    }
});

Meteor.publish('filterUser',function (rol) {

        return Meteor.users.find({roles:{$in:[rol]}});

});
Meteor.publish('users',function () {

        return Meteor.users.find({});

});
Meteor.publish('areasConocimiento',function () {
   return Areas.find({});
});
Meteor.publish('asignaturas',function (areas) {
    check(areas,String);


    return Asignaturas.find({area:areas});
});
Meteor.publish('todasAsignaturas',function () {

    return Asignaturas.find();
});
Meteor.publish('docentesAnual',function () {

    return DocentesAnual.find();
});
Meteor.publish('planAnual',function (id) {
    return PlanAnual.find({'autor.idDocente':id});
});
Meteor.publish('compartidoAnual',function (id,correo) {
    return PlanAnual.find({
        compartido: [
            {
                idDocente: id,
                correoElectronico: correo.emails[0].address
            }
        ], tipo: "anual"

    });
});
Meteor.publish('compartidoProyecto',function (id,correo) {

    return PlanAnual.find({
        compartido: [
            {
                idDocente: id,
                correoElectronico: correo.emails[0].address
            }
        ],tipo:"proyecto"

    });
});
Meteor.publish('compartidoElemental',function (id,correo) {
    return PlanAnual.find({
        compartido: [
            {
                idDocente: id,
                correoElectronico: correo.emails[0].address
            }
        ], tipo: "elemental"

    });
});
Meteor.publish('compartidoTrabajoCooperativo',function (id,correo) {
    return PlanAnual.find({
        compartido: [
            {
                idDocente: id,
                correoElectronico: correo.emails[0].address
            }
        ], tipo: "trabajoCooperativo"

    });
});
Meteor.publish('allplanAnual',function () {
    return PlanAnual.find({});
});
Meteor.publish('onePlanAnual',function (name) {
    return PlanAnual.find({nombre:name});
});
Meteor.publish('subnivel',function () {
    return Subnivel.find();
});
Meteor.publish('gradoCurso',function (seccion) {
    return GradoCurso.find({subnivel:seccion});
});
Meteor.publish('paralelo',function () {
    return Paralelo.find();
});
Meteor.publish('objetivosGeneralesFiltrados',function (area) {
    if(area!=null)
    return ObjetivosGenerales.find({area:area.prefix});
});
Meteor.publish('todosObjetivosGenerales',function () {
    return ObjetivosGenerales.find();
});
Meteor.publish('objetivosIntegradores',function () {
    return ObjetivosIntegradores.find();
});

Meteor.publish('objetivosIntegradoresFiltro',function (subnivel) {
    if(subnivel!=null)
    return ObjetivosIntegradores.find({subnivel:subnivel.index});
});

Meteor.publish('dimensionesIntegradores',function () {
    return DimensionesIntegradores.find();
});
Meteor.publish('numeroUnidad',function () {
    return NumeroUnidad.find();
});
Meteor.publish('unidad',function (id) {
    return Unidad.find({idPlanAnual:id});
});
Meteor.publish('unidadesPlan',function (id) {
    if(id!=null)
    return Unidad.find({idPlanAnual:id._id});
});
Meteor.publish('todasUnidades',function () {
    return Unidad.find();
});
Meteor.publish('objetivosEspecificos',function () {
    return ObjetivosEspecificos.find();
});

Meteor.publish('objetivosEspecificosFiltro',function (area,materia,subnivel) {
    if(area!=null&&materia!=null&&subnivel!=null)
    return ObjetivosEspecificos.find({subnivel:subnivel.index,area:area.prefix,materia:materia.text});
});
Meteor.publish('contenidosConceptuales',function () {
    return Contenidos.find();
});

Meteor.publish('contenidosConceptualesFiltro',function (area,materia,subnivel) {

    if(area!=null&&materia!=null&&subnivel!=null)
    return Contenidos.find({subnivel:subnivel.index,areaConocimiento:area.prefix,materia:materia.text});
});
Meteor.publish('contenidosConceptualesFiltroEF',function (area,materia) {

    if(area!=null&&materia!=null)
        return Contenidos.find({areaConocimiento:area.prefix,materia:materia.text});
});
Meteor.publish('destrezas',function () {
    return Destrezas.find();
});

Meteor.publish('destrezasFiltro',function (area,materia,subnivel) {
    if(area!=null&&materia!=null&&subnivel!=null)
    return Destrezas.find({subnivel:subnivel.index,area:area.prefix,materia:materia.text});
});
Meteor.publish('criteriosFiltro',function (area,materia,subnivel) {
    if(area!=null&&materia!=null&&subnivel!=null)
        return CriteriosEvaluacion.find({subnivel:subnivel.index,area:area.prefix,materia:materia});

});
Meteor.publish('criterioFilter',function (area,subnivel) {
    if(area!=null&&subnivel!=null)
    return CriteriosEvaluacion.find({subnivel:subnivel.index,area:area.prefix});

});
Meteor.publish('indicadoresFiltro',function (area,materia,subnivel) {
    if(area!=null&&materia!=null&&subnivel!=null)
        return IndicadoresEvaluacion.find({subnivel:subnivel.index,area:area.prefix,materia:materia.text});

});
Meteor.publish('neeData',function () {

        return NeeData.find({});

});
Meteor.publish('categoriaNEE',function () {

    return CategoriaNEE.find({});

});
Meteor.publish('diacData',function () {

    return DiacData.find({});

});
Meteor.publish('ejes',function () {
    return EjesDesarrolloElemental.find({});
});
Meteor.publish('curriculoIntegrador',function () {
    return CurriculoIntegradorElemental.find({});
});
Meteor.publish('reportData',function (id) {

    return ReportRoutes.find({idAutor:id});

});
Meteor.publish('allActividades',function () {

    return Actividades.find({});

});
Meteor.publish('allTecnicas',function () {

    return Tecnicas.find({});

});
Meteor.publish('allInstrumentos',function () {

    return Instrumentos.find({});

});
Meteor.publish('allContenidosUnidad',function () {

    return ContenidosConceptualesUnidad.find({});

});
Meteor.publish('contenidosConceptualesUnidadFilter',function (plan) {

    if(plan!=null)
    return ContenidosConceptualesUnidad.find({idPlanAnual:plan._id});

});
Meteor.publish('contenidosConceptualesUnidadFilterUnidad',function (unidad) {

    return ContenidosConceptualesUnidad.find({idUnidad:unidad._id});

});
Meteor.publish('contenidosConceptualesUnidad',function (idPlan) {
    return ContenidosConceptualesUnidad.find({idPlan:idPlan});
});

Meteor.publish('anioLectivo',function () {

    return AnioLectivo.find({});

});

    Meteor.publish('quimestre',function () {

    return Quimestre.find({});

});
Meteor.publish('metasComprension',function (idPlan) {
    return MetasComprension.find({idPlanAnual:idPlan});
});

Meteor.publish('inteligencias',function () {
    return Inteligencias.find({});
});

Meteor.publish('tipoActividad',function () {
    return TipoActividad.find({});
});
